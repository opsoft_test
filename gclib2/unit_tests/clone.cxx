/*
 * (c) Oleg Puchinin 2008
 * graycardinalster@gmail.com
 *
 */

#include <gclib2.h>

int i = 0;

int thread  (void * param)
{
	while (true) {
		printf ("%i\n", i);
		fflush (stdout);
	}
}

int main (int argc, char ** argv)
{
	Dclone (thread, NULL);
	while (true) { 
		++i;
		sleep (1);
	}
	
	return EXIT_SUCCESS;
}

