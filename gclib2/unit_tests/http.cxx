/*
 * (c) Oleg Puchinin 2008
 * graycardinalster@gmail.com
 *
 */

#include <gclib2.h>
#include <http.h>

int main (int argc, char ** argv)
{
	int size;
	char * m_data;
	
	m_data = http_get (argv[1], &size);
	fwrite (m_data, 1, size, stdout);
}

