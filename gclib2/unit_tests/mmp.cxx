/*
 * (c) Oleg Puchinin 2008
 * graycardinalster@gmail.com
 *
 */

#include <gclib2.h>
#include <MMP.h>

#define LOGIN "login"
#define PASS "pass"
#define MAILTO "mailto"

int main (int argc, char ** argv)
{
	MMP * m_mmp;

	m_mmp = new MMP;
	m_mmp->init ();

	if (m_mmp->login (LOGIN, PASS) < 0) {
		printf ("FAIL\n");
		return -1;
	}

	m_mmp->sendMessage (MAILTO, "OCHUMELIE RUCHKI !\n");
	
	delete m_mmp;
}

