/*
 * (c) Oleg Puchinin 2008
 * graycardinalster@gmail.com
 *
 */

#include <gclib2.h>
#include <gc_network.h>
#include <dns.h>

int thread_in (void * param)
{
	DConnection * c;
	Buf * b;
	c = (DConnection *) param;

	while (true) {
		c->select (30, 0);
		b = c->recv ();
		if (b) {
			write (1, b->data (), b->len ());
			delete b;
		}
	}

	return 0;
}

int main (int argc, char ** argv)
{
	Connection * c;
	char * server;
	char m_buf[4096];

	if (argc != 3) {
		printf ("usage: connection <IP> <PORT>\n");
		return EXIT_SUCCESS;
	}

	c = new Connection;
	c->init ();
	
	server = argv[1];
	if (! isIP (server))
		server = dns_A (server);
	if (c->connect (server, atoi (argv[2])) < 0) {
		perror ("connect");
		return EXIT_FAILURE;
	}

	Dclone (thread_in, (void *) c);

	while (fgets( m_buf, 256, stdin)) {
		chomp (m_buf);
		strcat (m_buf, "\n");
		c->send (m_buf, strlen (m_buf));
	}

	return EXIT_SUCCESS;
}

