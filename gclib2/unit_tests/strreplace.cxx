/*
 * (c) Oleg Puchinin 2008
 * graycardinalster@gmail.com
 *
 */

#include <gclib2.h>

char * datastr = "GOP STOP STOP STOP URA";

int main (int argc, char ** argv)
{
	char * m_buf;
	char * S;

	m_buf = CNEW (char, 128);
	strcpy (m_buf, datastr);
	S = strreplace (m_buf, "STOP", "SUP");
	printf ("%s\n", S);
	fflush (stdout);
	DROP (S);

	return EXIT_SUCCESS;
}

