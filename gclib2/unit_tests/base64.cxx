/*
 * (c) Oleg Puchinin 2008
 * graycardinalster@gmail.com
 *
 */

#include <gclib2.h>

int main (int argc, char ** argv)
{
	char * ptr;
	int size;

	ptr = DFILE ("./base64.cxx", &size);
	printf ("%s\n", base64_code ((uchar_t *) ptr, size));

	return EXIT_SUCCESS;
}

