/*
 * (c) Oleg Puchinin 2008
 * graycardinalster@gmail.com
 *
 */

#include <gclib2.h>

int m_sem;

int main (int argc, char ** argv)
{
	m_sem = sem_init (NULL);

	if (m_sem <= 0) {
		perror ("sem_init");
		return EXIT_FAILURE;
	}

	if (fork () == 0) {
		while (true) {
			down (m_sem);
			printf ("1\n"); fflush (stdout);
			up (m_sem);
		}
	} else {
		while (true) {
			down (m_sem);
			printf ("2\n", fflush (stdout));
			up (m_sem);
		}
	}


	return EXIT_SUCCESS;
}

