/*
 * (c) Oleg Puchinin 2008
 * graycardinalster@gmail.com
 *
 */

#include <gclib2.h>
#include <stdarg.h>
#include <Pkt.h>
#include "internals.h"

/*
 * a - char
 * b - int16_t
 * c - int
 * d - double
 *
 * e - Buf * (disabled)
 * s - строка с нулём на конце
 * l - список строк. 
 * L - бинарный список (disabled)
 * h - хэш
 * t - дерево (disabled)
 *
 */

char * __pack_bytes (char * ptr, char fmt, int arg)
{
	switch (fmt) {
		case 'a':
			pkt_W8 (&ptr, arg);
			break;
		case 'b':
			pkt_W16 (&ptr, arg);
			break;
		case 'c':
			pkt_W32 (&ptr, arg);
			break;
	}

	return ptr;
}

char * __pack_d (char * ptr, uint64_t dbl)
{
	((uint64_t *) ptr)[0] = dbl;
	return ptr += sizeof (uint64_t);
}

char * __pack_list (char * ptr, List * m_list)
{
	char * S;

	if (!ptr || !m_list)
		return NULL;

	pkt_W32 (&ptr, m_list->count ());
	m_list->first ();
	while (true) {
		S = m_list->get ();
		if (! S)
			break;
		ptr = strmov (ptr, S);
		++ptr;
		m_list->next ();
	}

	return ptr;
}

char * __pack (char * buf, int len, char * fmt, va_list ap)
{
	char * ptr;
	char * S;
	char * sparam;
	List * lparam;
	Hash * hparam;
	Buf * b;
	int param;
	uint64_t dbl;

	if (!buf || !fmt)
		return NULL; 

	ptr = buf;
	S = fmt;
	while (*S) {
		switch (*S) {
			case 's':
				sparam = va_arg (ap, char *);
				ptr = strmov (ptr, sparam);
				++ptr;
				break;

			case 'e':				// Buf *
				b =  va_arg (ap, Buf *);		
				if (! b) {
					pkt_W32 (&ptr, 0);
					break;
				}

				pkt_W32 (&ptr, b->len ());
				if (b->len ()) 
					pkt_WD (&ptr, b->data (), b->len ());
				break;

			case 'l':
				lparam = va_arg (ap, List *);
				ptr = __pack_list (ptr, lparam);
				break;

			case 'h':
				hparam = va_arg (ap, Hash *);
				ptr = __pack_list (ptr, hparam->keys ());
				ptr = __pack_list (ptr, hparam->values ());
				break;
		
			case 'a':
			case 'b':
			case 'c':
				param = va_arg (ap, int);
				ptr = __pack_bytes (ptr, *S, param);
				break;

			case 'd':
				dbl = va_arg (ap, uint64_t);
				ptr = __pack_d (ptr, dbl);
				break;
		}
		++S;
	}

	return ptr;
}

Buf * pack (char * fmt, ...)
{
	va_list ap;
	Buf * m_buf = NULL;
	int len;

	va_start (ap, fmt);
	len = pack_countBuf (fmt, ap);
	va_end (ap);

	m_buf = new Buf(len);

	va_start (ap, fmt);
	__pack (m_buf->data (), m_buf->len (), fmt, ap);
	va_end (ap);

	return m_buf;
}

