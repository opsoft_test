/*
 * (c) Oleg Puchinin 2006,2007
 * graycardinalster@gmail.com
 *
 */

#include <gclib2.h>
#include <termios.h>
#include <sys/types.h>
#include <sys/time.h>
#include <time.h>
#include <sys/time.h>
#define __export

struct timeval *cur_tv = NULL;

/*! Инициализировать таймер.
 * \note Функция не реентрантная.
 */
__export void Dtimer ()
{
	if (! cur_tv)
		cur_tv = (struct timeval *) malloc (sizeof (struct timeval));
	gettimeofday(cur_tv, NULL);
}

/*! \brief Время прощедшее с последнего запуска таймера.
 * \note Функция не реентрантная.
 */
__export struct timeval *the_time ()
{
	struct timeval new_tv;

	if (cur_tv == NULL)
		return NULL;
	
	gettimeofday (&new_tv, NULL);
	cur_tv->tv_sec = new_tv.tv_sec - cur_tv->tv_sec;
	if (new_tv.tv_usec >= cur_tv->tv_usec)
		cur_tv->tv_usec = new_tv.tv_usec - cur_tv->tv_usec;
	else {
		cur_tv->tv_sec--;
		cur_tv->tv_usec = cur_tv->tv_usec - new_tv.tv_usec;
	}

	return cur_tv;
}

/*! \brief Вывести строку со временем, прошедшим от запуска таймера.
 * \param file_my - поток для вывода строчки.
 * \note Функция не реентрантная.
 */
__export void print_the_time (FILE * file_my)
{
	if (! the_time ()) 
		return;
	
	if (file_my)
		fprintf (file_my, "The time : %i.%06i\n",
				(int) cur_tv->tv_sec,
				(int) cur_tv->tv_usec);
	else
		printf ("The time : %i.%06i\n",
			(int) cur_tv->tv_sec, (int) cur_tv->tv_usec);
	
}

/*! \brief Переключить терминал в режим посимвольного ввода.
 * \param fd - дескриптор, подключенный к терминалу.
 */
__export int Dterm_one_kick (int fd)
{
	struct termios ttystate;
	tcgetattr (fd, &ttystate);
	ttystate.c_lflag &= -ICANON;
	ttystate.c_cc[VMIN] = 1;
	return tcsetattr (fd, TCSANOW, &ttystate);
}

/// Вернуть строку с версией библиотеки GCLib.
__export char *Dversion ()
{
	return "2.5";
}

/*! \brief Заполнить буфер текущей датой и временем.
 * \param buf - буфер для заполнения.
 * \param max - размер буфера.
 */
__export char * Dtimestr (char * buf, int max)
{
	time_t t;
	time (&t);
	if (! buf)
		return NULL;
	strftime (buf, max, "%H:%M:%S %d.%m.%Y", localtime (&t)); 
	return buf;
}

