/*
 * (c) Oleg Puchinin 2007
 * graycardinalster@gmail.com
 *
 */

#include <gclib2.h>
#include <fcntl.h>
#include <sys/wait.h>

__export void Dexec_init (struct __djob_t * ctx)
{
	memset (ctx, 0, sizeof (struct __djob_t));
	ctx->stdIn = -1;
	ctx->stdOut = -1;
	ctx->stdErr = -1;
	ctx->pipe_in[0] = -1;
	ctx->pipe_in[1] = -1;
	ctx->pipe_out[0] = -1;
	ctx->pipe_out[1] = -1;
	ctx->pipe_err[0] = -1;
	ctx->pipe_err[1] = -1;
	ctx->otmp_name = (char *) malloc (128);
	ctx->etmp_name = (char *) malloc (128);
	ctx->otmp_name[0] = '\0';
	ctx->etmp_name[0] = '\0';
	ctx->shared_mem = NULL;
	ctx->pid = 0;
}

void __dexec_init_pipes (__djob_t * ctx, uint32_t opts)
{
	if (! ctx)
		return;
	if (opts & DEXEC_IPIPE)
		pipe (ctx->pipe_in);
	if (opts & DEXEC_OPIPE)
		pipe (ctx->pipe_out);
	if (opts & DEXEC_EPIPE)
		pipe (ctx->pipe_err);
}

void __dexec_parent (__djob_t * ctx, int opts, char * cmd)
{
	int status;

	if (! ctx)
		return;
	if (opts & DEXEC_IPIPE)
		fdclose (&ctx->pipe_in[0]);
	if (opts & DEXEC_OPIPE)
		fdclose (&ctx->pipe_out[1]);
	if (opts & DEXEC_EPIPE)
		fdclose (&ctx->pipe_err[1]);
	if (opts & DEXEC_WAIT) 
		waitpid (ctx->pid, &status, 0);
}

void __dexec_child (__djob_t * ctx, int opts, char * cmd)
{
	int fd;

	if (! ctx)
		return;
	
	if (opts & DEXEC_IPIPE) {
		fdclose (&ctx->pipe_in[1]);
		dup2 (ctx->pipe_in[0], fileno (stdin));
	}
	
	if (opts & DEXEC_OPIPE) {
		fdclose (&ctx->pipe_out[0]);
		dup2 (ctx->pipe_out[1], fileno (stdout));
	}

	if (opts & DEXEC_EPIPE) {
		fdclose (&ctx->pipe_err[0]);
		dup2 (ctx->pipe_err[1], fileno (stderr));
	}

	if (opts & DEXEC_INULL) {
		fd = open ("/dev/null", O_RDONLY);
		dup2 (fd, fileno (stdin));
	}

	if (opts & DEXEC_ONULL) {
		fd = open ("/dev/null", O_WRONLY);
		dup2 (fd, fileno (stdout));
	}

	if (opts & DEXEC_ENULL) {
		fd = open ("/dev/null", O_WRONLY);
		dup2 (fd, fileno (stderr));
	}

	if (cmd)
		exit (execlp ("/bin/sh", "/bin/sh", "-c", cmd, NULL));
}

__export __djob_t * Dexec (unsigned int opts, char * cmd)
{
	__djob_t * ctx;
	int fdo = -1;
	int fde = -1;

	ctx = CNEW (__djob_t, 1);
	Dexec_init (ctx);
	__dexec_init_pipes (ctx, opts);

	if (opts & DEXEC_OTMP) 
		fdo = Dtmpfd (ctx->otmp_name);
	if (opts & DEXEC_ETMP) 
		fde = Dtmpfd (ctx->etmp_name);

	ctx->pid = fork ();
	if (ctx->pid == 0) {
		if (opts & DEXEC_OTMP) {
			dup2 (fdo, fileno (stdout));
			fdclose (&fdo);
		}
		if (opts & DEXEC_ETMP) {			 
			dup2 (fde, fileno (stderr));
			fdclose (&fde);
		}
		__dexec_child (ctx, opts, cmd);
	} else if (ctx->pid > 0) {
		fdclose (&fdo);
		fdclose (&fde);
		__dexec_parent (ctx, opts, cmd);
		return ctx;
	} else
		return NULL;

	return ctx;
}

