/*
 * (c) Oleg Puchinin 2007,2008
 * graycardinalster@gmail.com
 *
 */

#include <gclib2.h>
#include "best_names.h"
#include <hv.h>

void __hv_free_item (char * ptr)
{
	ptr -= sizeof (char *);
	free (ptr);
}

HV::HV ()
{
	elements = new Array(4);
	dirty_elements = new EArray(1024);
	b_ac = true;
	dfa = 0;	
}

HV::~HV ()
{
	elements->foreach ((Dfunc_t) __hv_free_item);
	dirty_elements->foreach ((Dfunc_t) __hv_free_item);
	delete elements;
	delete dirty_elements;
}

char * HV::pack_item (char * key, char * value)
{
	char * pack;
	if (! key || ! value)
		return NULL;
	pack = CNEW (char, strlen (key) + 1 + sizeof (char *));
	*((char **) pack) = value;
	pack += sizeof (char *);
	strcpy (pack, key);
	return pack;
}

dkey_t * HV::unpackItem (char * pack, dkey_t * item)
{
	if (! pack || ! item)
		return NULL;
	item->KEY = pack;
	pack -= sizeof (char *);
	pack = *((char **) pack);
	item->VALUE = pack;
	return item;
}

char * HV::set_item_value (char * pack, char * value)
{
	pack -= sizeof (char *);
	*((char **) pack) = value;
	return value;
}

char * HV::get_item (char * key)
{
	char * ptr;
	int count;
	int i;

	if (! key)
		return NULL;

	i = elements->snfind_fast (key, 0);
	if (i >= 0) 
		return elements->get (i);

	count = dirty_elements->get_size ();
	for (i = 0; i < count; ++i) {
		ptr = dirty_elements->get (i);
		if (EQ (ptr, key))
			return ptr;
	}

	return NULL;
}

/// Получить значение по ключу.
char * HV::get (char * key)
{
	dkey_t one;
	if (unpackItem (get_item (key), &one)) 
		return one.VALUE;
	return NULL;
}

/// Установить значение ключа.
char * HV::set (char * key, char * value)
{
	char * pack;

	if (b_ac) {
		pack = get_item (key);
		if (pack) {
			set_item_value (pack, value);
			return value;
		}
	}

	dirty_elements->add (pack_item (key, value));
	if (dfa && (dirty_elements->get_size () >= dfa))
		do_sort (false);

	return value;
}

/// Сортировать ключи.
void HV::do_sort (bool b_clean)
{
	DHeapSort * heap;
	dkey_t item;
	char * S;
	int count;
	int dirty_count;
	int i;

	count = elements->get_size ();
	dirty_count = dirty_elements->get_size ();
	heap = new DHeapSort (count + dirty_count);
	for (i = 0; i < count; ++i) {
		if (b_clean) {
			unpackItem ((*elements)[i], &item);
			if (! item.VALUE)
				continue;
		}
		heap->add ((*elements)[i]);
	}

	for (i = 0; i < dirty_count; ++i) {
		S = dirty_elements->get (i);
		if (b_clean) {
			unpackItem (S, &item);
			if (item.VALUE)
				heap->add (S);
			else 
				delete (S - sizeof (char *));
		} else 
			heap->add (S);
	}

	delete elements;
	elements = new EArray (count + dirty_count);
	i = 0;
	while ((S = heap->extract_min ()) && S) { 
		++i;
		elements->add (S);
	}

	delete dirty_elements;
	dirty_elements = new EArray(1024);
	delete heap;
}

List * HV::keys_or_values (bool val)
{
	List * m_list;
	dkey_t one;
	int s;
	int i;

	m_list = new List;
	s = elements->get_size ();
	for (i = 0; i < s; ++i) {
		unpackItem (elements->get (i), &one);
		if (! val)
			m_list->add (one.KEY);
		else
			m_list->add (one.VALUE);
	}

	s = dirty_elements->get_size ();
	for (i = 0; i < s; ++i) {
		unpackItem (dirty_elements->get (i), &one);
		if (! val)
			m_list->add (one.KEY);
		else 
			m_list->add (one.VALUE);
	}
	
	return m_list;
}

List * HV::keys ()
{
	return keys_or_values (false);
}

List * HV::values ()
{
	return keys_or_values (true);
}

int HV::from_file (char *f_name)
{
	FILE * f;
	char * m_buf;
	char * d_set;

	m_buf = CNEW (char, 4096);
	f = fopen (f_name, "r");
	if (! f)
		return -1;

	while (true) {
		if (fgets (m_buf, 256, f) == NULL)
			break;
		
		if (m_buf[0] == '#')
			continue;

		chomp (m_buf);
		d_set = strchr (m_buf, '=');
		if (! d_set)
			continue;
		
		*d_set = '\0';
		++d_set;
		strip (d_set);
		strip2 (m_buf);
		set (m_buf, strdup (d_set));
	}

	fclose (f);
	DROP (m_buf);
	return 0;
}


void HV::foreach (Dhash_f f)
{
	List * m_keys;
	List * m_values;
	char * k;
	char * v;

	m_keys = keys ();
	m_values = values ();

	if (!m_keys || !m_values)
		return;

	m_keys->first ();
	m_values->first ();
	while (true) {
		k = m_keys->get ();
		v = m_values->get ();
		f (k, v);

		if (m_keys->next () == NULL)
			break;

		m_values->next ();
	}
	
	delete m_keys;
	delete m_values;
}

char * HV::operator [] (char *S)
{
	return get (S);
}

void HV::dump ()
{
	List * m_keys;
	List * m_values;

	m_keys = keys ();
	m_values = values ();

	if (!m_keys || !m_values)
		return;

	m_keys->first ();
	m_values->first ();
	while (m_keys->get ()) {
		printf ("%s => %s\n", m_keys->get (), m_values->get ());
		m_keys->next ();
		m_values->next ();
	}


	delete m_keys;
	delete m_values;
}

