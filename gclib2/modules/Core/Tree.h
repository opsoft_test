/*
 * (c) Oleg Puchinin 2007
 * graycardinalster@gmail.com
 *
 */

#ifndef DEFINE_TREE_H
#define DEFINE_TREE_H

#include "elist.h"
struct node_t;
struct node_t
{
	char * key;
	char * userData;
	uint32_t ID;
	node_t * parentNode;
	List * childNodes; // node_t *
};

typedef void (node_f) (node_t *);

/// Класс - дерево.
class Tree 
{
	public:
		Tree ();
		~Tree ();

		node_t * rootNode;
		char * userData;

		node_t * newNode (node_t * parent, char * key = NULL, char * userData = NULL);
		char * freeNode (node_t * node, Dfunc_t f);
		List * childs (node_t * node);
		node_t * searchDown (node_t * node, char *key, uint32_t ID = 0);
		node_t * searchUp (node_t * node, char *key, uint32_t ID = 0);
		List * keyChilds (node_t * node, char * key, uint32_t ID = 0);
		void foreach (node_f fn, node_t * m_node = NULL);
	
	private:
		uint16_t lastID;
};

#endif
