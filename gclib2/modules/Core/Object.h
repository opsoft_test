/*
 * (c) Oleg Puchinin 2008
 * graycardinalster@gmail.com
 *
 */

#ifndef DEFINE_OBJECT_H
#define DEFINE_OBJECT_H

#include <Buf.h>

class Object
{
	public:
		Object ();
		~Object ();

		virtual Buf * pack ();
		virtual Object * unpack (Buf *);
};

#endif

