/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#ifndef DEFINE_ELIST_H
#define DEFINE_ELIST_H

#include "dlist.h"

#ifdef __cplusplus

/// Класс - список.
class EList : public DList 
{
	public:
		EList ();
		~EList ();
		
		char * get ();
		char * first ();
		char * last ();
		char * next ();
		char * prev ();
		char * rm ();

		// v2.2
		char * get_first ();
		char * get_last (); 
		char * add_before (char * S); 
		char * add_after (char * S); 
		char * shift (); 
		char * unshift (char * S);
		char * push (char *S);
		char * pop ();
		void dump ();
		void bdump ();
		__dlist_entry_t * get_entry ();
		Array * toArray ();
		/**/

		bool eol ();
		inline char * add (char *S) {
			return add_tail (S);
		}
		EList & operator << (char *);
		char * operator [] (int offset);
		List & operator = (List &);
		bool operator == (List &);
		bool operator != (List &);
		List & operator + (List & l);
		
	private:
		__dlist_entry_t * pos;
};
#endif

#endif

