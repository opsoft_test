/*
 * (c) Oleg Puchinin 2006,2007
 * graycardinalster@gmail.com
 *
 */

/*! \brief Простое шифрование через XOR
 * \param data - входные данные.
 * \param size - размер входных данных.
 * \param key - ключ для шифровки/дешифровки.
 * \param key_size - размер ключа.
 */
void bxor (char * data, int size, char *key, int key_size)
{
	int i,k;
	k = 0;
	
	for (i = 0; i < size; ++i || ++k) {
		if (k == key_size)
			k = 0;
		data[i] ^= key[k];
	}		
}

