/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "gclib2.h"

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#ifndef HAVE_OPENSSL
unsigned char * Ddigest (void * data, int len, char *name, unsigned int * out_len)
{
	if (out_len)
		*out_len = 0;
	return NULL;
}
#else
#include <openssl/evp.h>
/*! \brief Посчитать хэш (к примеру - MD5)
 * \param data - исходные данные.
 * \param len - длина исходных данных.
 * \param name - тип хэша (к примеру - "MD5").
 * \param out_len - результирующий размер хэша.
 * \note Результирующий хэш НЕ null-terminated string.
 */
unsigned char * Ddigest (void * data, int len, char *name, 
				unsigned int * out_len)
{
	EVP_MD_CTX mdctx;
	const EVP_MD *md;
	unsigned char * digest;;
	unsigned int md_len;
	
	if (!data || !name)
		return NULL;
	
	OpenSSL_add_all_digests();
	md = EVP_get_digestbyname(name);
		
	EVP_MD_CTX_init(&mdctx);
	EVP_DigestInit_ex(&mdctx, md, NULL);
	EVP_DigestUpdate(&mdctx, data, len);
	digest = CNEW (unsigned char, EVP_MAX_MD_SIZE);
	EVP_DigestFinal_ex(&mdctx, digest, &md_len);
	EVP_MD_CTX_cleanup(&mdctx);
	
	if (out_len)
		*out_len = md_len;

	return digest;
}

#endif

