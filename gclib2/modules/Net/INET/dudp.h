/*
 * (c) Oleg Puchinin 2007
 * graycardinalster@gmail.com
 *
 */

#ifndef DEFINE_DUDP_H
#define DEFINE_DUDP_H

#ifndef DEFINE_DPBUF_H
#include <dpbuf.h>
#endif

class DUdp : public DPBuf
{
	public:
		DUdp ();
		~DUdp ();
		
		int init (char * data, int len, char * IP, uint16_t PORT);
		char * ip;
		uint16_t port;
};

#endif

