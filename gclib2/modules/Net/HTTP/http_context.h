/*
 * (c) Oleg Puchinin 2007
 * graycardinalster@gmail.com
 *
 */

#ifndef DEFINE_CONNECTION_CONTEXT_H
#define DEFINE_CONNECTION_CONTEXT_H

struct http_context 
{
	char * file;
	char * serverName;
	char * serverIP;
	int serverSd;

	int retCode;
	int resFd;
	int resOffset;
	char resFileName[256];

};

http_context * http_context_alloc ();
int http_context_clean (http_context * ctx);

#endif

