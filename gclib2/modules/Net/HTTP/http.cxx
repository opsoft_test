/*
 * (c) Oleg Puchinin 2007,2008
 * graycardinalster@gmail.com
 *
 */

#include <gclib2.h>
#include <dns.h>
#include <sys/socket.h>
#include <time.h>
#include "http.h"
#include "internals.h"
#include "http_context.h"
#include <gc_network.h>

int http_send_request (http_context * ctx)
{
	int Ret;
	char m_buf[512];
	
	if (! ctx)
		return -1;
	sprintf (m_buf, "GET %s HTTP/1.1\r\nHost: %s\r\n\r\n", ctx->file, ctx->serverName);
	Ret = send (ctx->serverSd, m_buf, strlen (m_buf), 0);
	shutdown (ctx->serverSd, 1);

	return Ret;
}

int http_write_header (http_context * ctx, int tmpfd)
{
	char * m_header;
	
	if (! ctx)
		return -1;
	m_header = CNEW (char, ctx->resOffset);
	read (ctx->resFd, m_header, ctx->resOffset);
	write (tmpfd, m_header, ctx->resOffset);
	DROP (m_header);

	return 0;
}

int http_chunked_result_join (http_context * ctx)
{
	char tmpfilename[256];
	char * m_buf;
	int m_tmpfd;
	uint32_t count = 0;
	FILE * m_oldfile;
	FILE * m_newfile;
		
	if (! ctx)
		return -1;
	

	m_tmpfd = Dtmpfd (tmpfilename);
	if (m_tmpfd < 0) 
		return -1;

	http_write_header (ctx, m_tmpfd);

	m_oldfile = fdopen (ctx->resFd, "r");
	if (m_oldfile == NULL) 
		return -1;

	fseek (m_oldfile, ctx->resOffset, SEEK_SET);
	m_newfile = fdopen (m_tmpfd, "r+");

	m_buf = CNEW (char, 4096);
	while (true) {
		m_buf[0] = 0;
		fgets (m_buf, 4096, m_oldfile);
		if (feof (m_oldfile))
			break;

		sscanf (m_buf, "%x", &count); 
		if (count == 0)
			break;
		fread (m_buf, 1, 2, m_oldfile);
		Dcopyfile (m_oldfile, m_newfile, count);
	}
	fclose (m_oldfile);
	fclose (m_newfile);
	DROP (m_buf);
	
	unlink (ctx->resFileName);
	ctx->resFd = open (tmpfilename, O_RDONLY | O_LARGEFILE);
	strcpy (ctx->resFileName, tmpfilename);
	lseek (ctx->resFd, ctx->resOffset, SEEK_SET);
	
	return 0;
}

const char * TransferEncoding =  "Transfer-Encoding: ";

int http_parse_result (http_context * ctx)
{
	char m_buf[512];
	bool m_chunked;
	char * S;
	int header_len = 0;
	FILE * m_resFile;

	if (! ctx)
		return -1;

	lseek (ctx->resFd, 0, SEEK_SET);
	m_resFile = fdopen (ctx->resFd, "r+");
	fgets (m_buf, 512, m_resFile);
	header_len = strlen (m_buf);
	
	sscanf (m_buf, "HTTP/1.1 %d", &ctx->retCode);
	m_chunked = false;
	while (fgets (m_buf, 512, m_resFile)) {
		header_len += strlen (m_buf);
		chomp (m_buf);
		if (strlen (m_buf) == 0)
			break;

		if (! strncmp (m_buf, TransferEncoding, strlen (TransferEncoding))) {
			S = strstr (m_buf, ": ");
			S += 2;
			if (EQ (S, "chunked"))
				m_chunked = true;
		}
	}
	fclose (m_resFile);
	ctx->resFd = open (ctx->resFileName, O_RDONLY);
	ctx->resOffset = header_len;

	if (m_chunked) 
	       http_chunked_result_join (ctx);
	
	return 0;
}

http_context * http_context_alloc ()
{
	http_context * ctx;
	ctx = CNEW (http_context, 1);
	memset (ctx, 0, sizeof (http_context));
	ctx->serverSd = -1;
	ctx->resFd = -1;
	return ctx;
}

int http_context_clean (http_context * ctx)
{
	if (! ctx)
		return -1;

	fdclose (&ctx->serverSd);
	fdclose (&ctx->resFd);
	
	DROP (ctx->file);
	DROP (ctx->serverName);
	DROP (ctx->serverIP);
	if (strlen (ctx->resFileName))
		unlink (ctx->resFileName);
	DROP (ctx);

	return 0;
}

int http_connect (http_context * ctx)
{
	if (! ctx)
		return -1;

	if (isIP (ctx->serverName)) 
		ctx->serverIP = strdup (ctx->serverName);
	else
		ctx->serverIP = dns_A (ctx->serverName);
	
	if (! ctx->serverIP)
		return -1;

	ctx->serverSd = dSocket ();
	if (ctx->serverSd < 0)
		return -1;
	if (dConnect (ctx->serverSd, ctx->serverIP, 80)) {
		close (ctx->serverSd);
		return -1;
	}

	return ctx->serverSd;
}

int http_send_request (int sd, char * m_server, char * m_link)
{
	int Ret;
	char m_buf[512];
	sprintf (m_buf, "GET %s HTTP/1.1\r\nHost: %s\r\n\r\n", m_link, m_server);
	Ret = send (sd, m_buf, strlen (m_buf), 0);
	shutdown (sd, 1);
	return Ret;
}

int http_recv_response (http_context * ctx)
{
	srand (time (NULL));

	if (! ctx)
		return -1;

	ctx->resFd = Dtmpfd (ctx->resFileName);
	if (ctx->resFd < 0)
		return -1;
	
	move_stream (ctx->serverSd, ctx->resFd);
	lseek (ctx->resFd, 0, SEEK_SET);

	return ctx->resFd;
}

http_context * __http_get (char * addr)
{
	http_context * ctx;
	char m_buf[512];
	char * S;

	if (! addr)
		return NULL;

	ctx = http_context_alloc ();
	strcpy (m_buf, addr);
	S = strstr (m_buf, "://");
	if (S) {
		*S = '\0';
		if (NE (m_buf, "http"))
			return NULL;
		S += 3;
		strcpy (m_buf, S);
	}

	S = strchr (m_buf, '/');
	if (S) {
		ctx->file = strdup (S);
		*S = '\0';
	} else
		ctx->file = strdup ("/");

	ctx->serverName = strdup (m_buf);

	if (http_connect (ctx) < 0)
		goto httperrout;

	if (http_send_request (ctx) < 0)
		goto httperrout;

	if (http_recv_response (ctx) < 0)
		goto httperrout;

	if (http_parse_result (ctx) < 0)
		goto httperrout;

	return ctx;

httperrout:
	http_context_clean (ctx);
	return NULL;
}

char * http_get (char * url, int * m_size)
{
	http_context * ctx;
	char * m_buf;

	ctx = __http_get (url);
	if (! ctx) {
		if (m_size)
			*m_size = 0;
		return NULL;
	}

	lseek (ctx->resFd, ctx->resOffset, SEEK_SET);
	m_buf = Dread_to_eof (ctx->resFd, m_size);
	http_context_clean (ctx);
	return m_buf;
}

