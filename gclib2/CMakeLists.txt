#set (CMAKE_VERBOSE_MAKEFILE true)

message("
Configuration
-------------
CMAKE_BUILD_TYPE        = ${CMAKE_BUILD_TYPE}
CXXFLAGS                = $ENV{CXXFLAGS}
CMAKE_CXX_FLAGS         = ${CMAKE_CXX_FLAGS}
LDFLAGS                 = $ENV{LDFLAGS}
CMAKE_EXE_LINKER_FLAGS  = ${CMAKE_EXE_LINKER_FLAGS}
CMAKE_INSTALL_PREFIX    = ${CMAKE_INSTALL_PREFIX}
")

include_directories ("include")
project ("gclib2")
add_library (gclib2 SHARED modules/Core/String.cxx
	modules/Core/dexec.cxx
	modules/Core/dpbuf.cxx
	modules/Core/earray.cxx
	modules/Core/hv.cxx
	modules/Core/djobs.cxx
	modules/Core/Tree.cxx
	modules/Core/elist.cxx
	modules/Core/darray.cxx
	modules/Core/Buf.cxx
	modules/Core/Object.cxx
	modules/Core/dlist.cxx
	modules/Misc/strings.cxx
	modules/Misc/dsplit.cxx
	modules/Misc/join.cxx
	modules/Misc/dheapsort.cxx
	modules/Misc/Pkt.cxx
	modules/Misc/misc.cxx
	modules/Misc/Pack/pack.cxx
	modules/Misc/Pack/unpack.cxx
	modules/Misc/Pack/unpack_listhash.cxx
	modules/Misc/Pack/count.cxx
	modules/Misc/Pack/pack_countList.cxx
	modules/Misc/Pack/pack_countBList.cxx
	modules/Misc/Pack/pack_countHash.cxx
	modules/Misc/Pack/pack_countTree.cxx
	modules/Misc/split.cxx
	modules/Linux/linux_specific.cxx
	modules/IPC/ipc.cxx
	modules/Net/FTP/FTP.cxx
	modules/Net/HTTP/http.cxx
	modules/Net/INET/network.cxx
	modules/Net/INET/dudp.cxx
	modules/Net/INET/dconnection.cxx
	modules/Net/INET/url.cxx
	modules/Net/INET/Poll.cxx
	modules/Net/DNS/dns.cxx
	modules/Net/MMP/mmp_engine.cxx
	modules/IO/IO.cxx
	modules/IO/dfiles.cxx
	modules/Crypt/digests.cxx
	modules/Crypt/scode.cxx
	modules/Crypt/base64.cxx )
