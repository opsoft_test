/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 * 
 */

#ifndef DEFINE_BUF_H
#define DEFINE_BUF_H

#include <dpbuf.h>

class Buf;
class Buf : public DPBuf
{
	public:
		Buf ();
		Buf (int len);
		Buf (char * ptr, int len);
		~Buf ();

		char * seek (int offset, int whence);
		int available ();
		char * position ();
		Buf * shift (int count);
		int unshift (Buf * m_buf);

		char * readString (); // v2.3
		
		bool operator == (Buf * m_buf);
		bool operator != (Buf * m_buf);
};

void buf_free (void * ptr);

#endif

