/*
 * (c) Oleg Puchinin 2006,2007
 * graycardinalster@gmail.com
 * 
 */

#ifndef DEFINE_DCONNECTION_H
#define DEFINE_DCONNECTION_H

#include <fcntl.h>

#ifndef DEFINE_DUDP_H
#include <dudp.h>
#endif

class DConnection;
/// Класс "соединение".
class DConnection
{
        public:
                DConnection ();
		DConnection (int sd);
                ~DConnection ();

                int init ();
                int initUdp ();
		int reuseAddr ();
                int bind (char *ip, uint16_t port);
                int connect (char *ip, uint16_t port = 0);
                int connectUdp (char *ip, uint16_t port);
		int listen (int N);
		Connection * accept ();
                int send (char * buf, int len);
                int sendTo (char * buf, int len);
                int recv (char * buf, int len);
		int recvFrom (char *buf, int len, char * IP, uint16_t * PORT);
		DUdp * recvUdp (int bufLen = 65535);
                void updateCTime (time_t d_time = 0);
                void updateMTime (time_t d_time = 0);
                int ioNRead ();
		int select (int secs, int usecs);
                Connection * clone ();
                int setBroadcast ();
		int setSocket (int m_sd, bool dirty = true);
		int socket ();
		int setTimeOut (int t);
		int timeout ();
		int close ();

		void setName (char * m_name);
		void setGroup (char * m_group);
		void setRevents (int m_set);
		void setPollFlags (int m_set);

                int open (char *f_name, int openmode, int filemode = 0644);
                int read (char *m_buf, int len);
                int write (char *m_buf, int len);
                char * getsockname ();
                char * getpeername ();
                uint16_t getpeerport ();
                uint16_t getsockport ();
                time_t get_ctime ();
                time_t get_mtime ();
		bool broadcast ();

		char * name ();
		char * group ();
		int revents ();
		int pollFlags ();

		/* v2.3 */
		Buf * read ();
		Buf * recv (int opts = 0);
		int write (Buf *);
		int send (Buf *, int opts = 0);
		List * recvStrings ();
		int shutdown (int how);
		char * userData ();
		char * setUserData (char * ptr);

		/**/

	private:
		char * c_user_data;	
		int c_sd;
		int c_type;
		char * c_name;
		char * c_group;
		time_t c_mtime;
		time_t c_ctime;
		char * c_cname; // client address
		char * c_pname; // server address                
		uint16_t c_cport;
		uint16_t c_pport;
		char * c_status; // other data
		int c_poll_flags;                
		int c_revents;
		bool c_bcast;
		bool dirtySocket;
		int c_timeout;
		void prepare ();
};

#endif

