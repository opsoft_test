/*
 * (c) Oleg Puchinin 2008
 * graycardinalster@gmail.com
 *
 */

#ifndef DEFINE_GC_IO_H
#define DEFINE_GC_IO_H

/*** ./IO.cxx ***/
__export int Dfnwrite (char * p_lpsz_filename, void * p_lp_buffer,int int_size) ;
__export int Dfnread (char * f_name, void * p_lp_buffer, int int_size) ;
__export int Dselect (int FILENO, int SEC, int USEC) ;
__export char * DFILE (const char * m_filename, int *rsize) ;
__export struct stat * DSTAT (const char * S) ;
__export struct stat * DLSTAT (const char * S) ;
__export int DIONREAD (int fd) ;
__export int fsize (const char * S) ;
__export int fdsize (int fd) ;
__export char * DFDMAP (int fd) ;
__export char * DFMAP (const char *d_file, int *out_fd, int *d_out_size) ;
__export char * Dread_to_eof (int fd, int *d_out_size) ;
__export char * allData (int fd, int * d_out_size) ;
__export int move_stream (int fd_in, int fd_out) ;
__export int move_stream_file (FILE * m_src, FILE * m_dst) ;
__export int Dnonblock (int fd) ;
__export int close_pipe (int *fds) ;
__export int Dtmpfd (char *name) ;
__export FILE * Dtmpfile (char *name) ;
__export int fdclose (int * fd) ;
__export char * fext (char *name) ;
__export int logToFile (char * fileName, char * fmt, ...) ;
__export int copyFile (char * sourceName, char * destName) ;
__export char * DSTR (FILE * m_file) ;
__export int Dcopyfd (int m_source, int m_dest, int N) ;
__export int Dcopyfile (FILE * m_source, FILE * m_dest, int N) ;

__export List * file (char * fileName); // v2.2

/*** ./dfiles.cxx ***/
__export List * Dfiles (char * path) ;

#endif

