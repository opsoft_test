/*
 * (c) Oleg Puchinin 2008
 * graycardinalster@gmail.com
 *
 */

#ifndef DEFINE_NETWORK_H
#define DEFINE_NETWORK_H

#include <stdint.h>
#include <netinet/in.h>
#include <arpa/inet.h>

__export int sendToPath(void * lp_data,int int_size,char * lpsz_path) ;
__export int sendToSocket (int sock, void * lp_data, int int_size, const char * address, uint16_t port) ;
__export int sendTo (void * lp_data, int int_size, const char * address, uint16_t port);
__export int dBind (int sock, char * addr, int port) ;
__export int dSocket () ;
__export int dUdpSocket () ;
__export int dConnect (int sock, const char * addr, uint16_t port) ;
__export int getHostByName(const char * hostName,struct in_addr * address) ;
__export char * getNameByHost(struct in_addr * addr) ;
__export int recvFrom (int sd, char * buf, int size, char * peer_ip, uint16_t * peer_port) ;
__export int setBroadcast (int fd) ; 
__export int pollScan (struct pollfd *p, int count, int pos) ;

__export char * url_protocol (char * url) ;
__export char * url_host (char * url) ;
__export char * url_file (char * url) ;

#endif

