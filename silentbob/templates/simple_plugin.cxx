/*
 * (c) Oleg Puchinin 2007
 * graycardinalster@gmail.com
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <gclib/gclib.h>
#include <gclib/gclib_c.h>
#include <mod.h>
#include <head.h>
#include <dbg.h>

extern "C" DArray * plugin_init (struct env_t *env);
struct env_t *ENV;

char simple_plugin_opt (DArray * d_opts, int * pos)
{
	int count;
	char * S;
	
	if (! d_opts || ! pos)
		return 0;

	count = d_opts->get_size ();
	S = d_opts->get (*pos);
	if (EQ (S, "--simple_plugin")) {
		if (++(*pos) >= count) 
			return 0; // Missed argument for option "--simple-plugin".
		// Do somethings.
	}

	return 0;
}

char simple_plugin_opt2 (DArray * d_opts, int * pos)
{
	char * S;
	
	if (! d_opts || ! pos)
		return 0;
	
	S = d_opts->get (*pos);
	if (EQ (S, "--simple_plugin")) {
		// Do somethings at stage two.
	}

	return 0;
}

void simple_plugin_info ()
{
	printf ("Simple plugin.\n");
	printf ("Version: 1.0\n");
	printf ("options: --simple-plugin\n");
}

DArray * plugin_init (struct env_t *env)
{
	DArray * Ret;
	struct mod_feature * pm;

	ENV = env;
	Ret = new DArray (1);
	pm = CNEW (mod_feature, 1);
	memset (pm, 0, sizeof (mod_feature));
	pm->mod.Type = TYPE_FEATURE;
	pm->mod.Version = strdup ("1.0");
	pm->mod.short_info = simple_plugin_info;
	pm->opt = simple_plugin_opt;
	pm->opt2 = simple_plugin_opt2;

	Ret->add (LPCHAR (pm));
	return Ret;
}

