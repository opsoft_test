/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 * 
 */ 

#ifndef DEFINE_ENV_H
#define DEFINE_ENV_H

struct env_t
{
	EArray * d_tags_file;
	EArray * immune_list;
	EArray * full_list;
	EArray * modding;
	EArray * d_cops;
	DArray * d_opts;
	EArray * d_files;
	DJobs * proc_list;
	EHash * settings;
	EList * listOptions;
	
	FILE *d_stream_dbg;
	
	char * home_dir;
	char * tmp_files;
	char * tmp_tags;
	char * debug_file;
	char * home_plugins;
	char * shared_plugins;
	char * language;
	char * cgrep_exp;

	bool d_dbg_SDBG_active;
	int t_op_no;
	int d_depth;
	int cgrep_A;
	int cgrep_B;
	int sb_cmd;
	int max_proc;
	int nproc;
	int d_kinds;
	uint32_t sb_flags;
};

extern "C" struct env_t *ENV;
extern "C" struct env_t *get_env ();
#endif
