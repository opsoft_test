export CC="gcc"
export CXX="g++"
export CFLAGS="-O3 -Wall -pipe"
export CXXFLAGS="-O3 -Wall -pipe"
export INCLUDE="-I..//include -I..//gclib/include"
export OPTS="-fPIC -DPIC"
export LDFLAGS=""
export LIBS="-L./"

$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//gclib/src/darray.o ..//gclib/src/darray.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//gclib/src/deprecated_dsplit.o ..//gclib/src/deprecated_dsplit.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//gclib/src/dhash.o ..//gclib/src/dhash.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//gclib/src/dheapsort.o ..//gclib/src/dheapsort.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//gclib/src/djobs.o ..//gclib/src/djobs.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//gclib/src/dlist.o ..//gclib/src/dlist.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//gclib/src/dsplit.o ..//gclib/src/dsplit.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//gclib/src/earray.o ..//gclib/src/earray.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//gclib/src/ehash.o ..//gclib/src/ehash.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//gclib/src/elist.o ..//gclib/src/elist.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//gclib/src/fs.o ..//gclib/src/fs.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//gclib/src/gclib.o ..//gclib/src/gclib.cxx
$CC $CFLAGS $OPTS $INCLUDE -c -o ..//gclib/src/gclib_c.o ..//gclib/src/gclib_c.c
gcc $LDFLAGS $LIBS -shared ..//gclib/src/darray.o ..//gclib/src/deprecated_dsplit.o ..//gclib/src/dhash.o ..//gclib/src/dheapsort.o ..//gclib/src/djobs.o ..//gclib/src/dlist.o ..//gclib/src/dsplit.o ..//gclib/src/earray.o ..//gclib/src/ehash.o ..//gclib/src/elist.o ..//gclib/src/fs.o ..//gclib/src/gclib.o ..//gclib/src/gclib_c.o -o libgclib.so 
export CC="gcc"
export CXX="g++"
export CFLAGS="-O3 -Wall -pipe"
export CXXFLAGS="-O3 -Wall -pipe"
export INCLUDE="-I..//include -I..//gclib/include"
export OPTS="-fPIC -DPIC"
export LDFLAGS=""
export LIBS="-L./ -lsblib -lgclib -ldl"

$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//sblib/py_tt.o ..//sblib/py_tt.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//sblib/Sblib.o ..//sblib/Sblib.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//sblib/the_fly.o ..//sblib/the_fly.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//sblib/the_tt.o ..//sblib/the_tt.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//sblib/t_op2.o ..//sblib/t_op2.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//sblib/t_op.o ..//sblib/t_op.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//sblib/TT.o ..//sblib/TT.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//sblib/wit.o ..//sblib/wit.cxx
gcc $LDFLAGS $LIBS -shared ..//sblib/py_tt.o ..//sblib/Sblib.o ..//sblib/the_fly.o ..//sblib/the_tt.o ..//sblib/t_op2.o ..//sblib/t_op.o ..//sblib/TT.o ..//sblib/wit.o -o libsblib.so 

export CC="gcc"
export CXX="g++"
export CFLAGS="-O3 -Wall -pipe"
export CXXFLAGS="-O3 -Wall -pipe"
export INCLUDE="-I..//include -I..//gclib/include"
export OPTS=""
export LDFLAGS=""
export LIBS="-L./ -lsblib -lgclib -ldl"

$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//src/callTags.o ..//src/callTags.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//src/cFiles.o ..//src/cFiles.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//src/cgrep.o ..//src/cgrep.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//src/ctags.o ..//src/ctags.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//src/file.o ..//src/file.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//src/indent.o ..//src/indent.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//src/init.o ..//src/init.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//src/interactive.o ..//src/interactive.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//src/kinds.o ..//src/kinds.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//src/main.o ..//src/main.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//src/modding.o ..//src/modding.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//src/opts_funcs.o ..//src/opts_funcs.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//src/opts_kinds.o ..//src/opts_kinds.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//src/opts_list.o ..//src/opts_list.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//src/opts_settings.o ..//src/opts_settings.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//src/structs.o ..//src/structs.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//src/tree.o ..//src/tree.cxx
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//src/usage.o ..//src/usage.cxx
gcc $LDFLAGS $LIBS ..//src/callTags.o ..//src/cFiles.o ..//src/cgrep.o ..//src/ctags.o ..//src/file.o ..//src/indent.o ..//src/init.o ..//src/interactive.o ..//src/kinds.o ..//src/main.o ..//src/modding.o ..//src/opts_funcs.o ..//src/opts_kinds.o ..//src/opts_list.o ..//src/opts_settings.o ..//src/structs.o ..//src/tree.o ..//src/usage.o -o silent_bob 
export CC="gcc"
export CXX="g++"
export CFLAGS="-O3 -Wall -pipe"
export CXXFLAGS="-O3 -Wall -pipe"
export INCLUDE="-I..//include -I..//gclib/include"
export OPTS="-fPIC -DPIC"
export LDFLAGS=""
export LIBS="-L./ -lsblib -lgclib -ldl"

$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//plugins/plugin_perl.o ..//plugins/plugin_perl.cxx
gcc $LDFLAGS $LIBS -shared ..//plugins/plugin_perl.o -o libplugin_perl.so 
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//plugins/plugin_perlpackages.o ..//plugins/plugin_perlpackages.cxx
gcc $LDFLAGS $LIBS -shared ..//plugins/plugin_perlpackages.o -o libplugin_perlpackages.so 
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//plugins/plugin_cache.o ..//plugins/plugin_cache.cxx
gcc $LDFLAGS $LIBS -shared ..//plugins/plugin_cache.o -o libplugin_cache.so 
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//plugins/plugin_editor.o ..//plugins/plugin_editor.cxx
gcc $LDFLAGS $LIBS -shared ..//plugins/plugin_editor.o -o libplugin_editor.so 
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//plugins/plugin_grep.o ..//plugins/plugin_grep.cxx
gcc $LDFLAGS $LIBS -shared ..//plugins/plugin_grep.o -o libplugin_grep.so 
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//plugins/plugin_compile.o ..//plugins/plugin_compile.cxx
gcc $LDFLAGS $LIBS -shared ..//plugins/plugin_compile.o -o libplugin_compile.so 
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//plugins/plugin_aap.o ..//plugins/plugin_aap.cxx
gcc $LDFLAGS $LIBS -shared ..//plugins/plugin_aap.o -o libplugin_aap.so 
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//plugins/plugin_python.o ..//plugins/plugin_python.cxx
gcc $LDFLAGS $LIBS -shared ..//plugins/plugin_python.o -o libplugin_python.so 
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//plugins/plugin_scons.o ..//plugins/plugin_scons.cxx
gcc $LDFLAGS $LIBS -shared ..//plugins/plugin_scons.o -o libplugin_scons.so 
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//plugins/plugin_foreach.o ..//plugins/plugin_foreach.cxx
gcc $LDFLAGS $LIBS -shared ..//plugins/plugin_foreach.o -o libplugin_foreach.so 
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//plugins/plugin_make.o ..//plugins/plugin_make.cxx
gcc $LDFLAGS $LIBS -shared ..//plugins/plugin_make.o -o libplugin_make.so 
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//plugins/plugin_xml_project.o ..//plugins/plugin_xml_project.cxx
gcc $LDFLAGS $LIBS -shared ..//plugins/plugin_xml_project.o -o libplugin_xml_project.so 
$CXX $CXXFLAGS $OPTS $INCLUDE -c -o ..//plugins/plugin_csharp.o ..//plugins/plugin_csharp.cxx
gcc $LDFLAGS $LIBS -shared ..//plugins/plugin_csharp.o -o libplugin_csharp.so 
