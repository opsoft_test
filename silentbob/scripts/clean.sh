#!/bin/bash

CLEAN_MASK="*.o *.so *.os *.lo *.la .deps .libs"

make distclean
cd src
rm -fr $CLEAN_MASK
cd ..
rm -f ./silent_bob
rm -f src/silent_bob
rm -fr ./autom4te.cache
cd sblib
rm -fr $CLEAN_MASK
cd ../gclib/src
rm -fr $CLEAN_MASK
cd ../gclib_c
rm -fr $CLEAN_MASK
cd ../../plugins
rm -fr $CLEAN_MASK
cd ../
rm -fr ./tags
rm -fr ./cfiles
rm -fr debug/*
rm -fr package/*
rm -fr debian/silentbob/*
cd scripts
scons -c
aap cleanALL
cd ../gclib/scripts
scons -c
aap cleanALL
