/*
 * (c) Oleg Puchinin 2006,2007
 * graycardinalster@gmail.com
 * 
 */ 

#include <head.h>
#include <wit.h>
#include <the_tt.h>
#include <dbg.h>
#include <fcntl.h>
#include <TT.h>

int callTagsFile (char * fileName)
{
	char * functionName = NULL;
	char m_name[256];
	char * oldName = NULL;
	char * ptr = NULL;
	bool b_inmacro = false;
	char * S = NULL;
	int i;
	TT * tt = NULL;
	DArray * d_words = NULL;

	tt = new TT;
	tt->loadFile (fileName);
	tt->init ();

	while (true) {
		tt->nextOperator ();
		if (tt->ch == 0)
			break;

		ptr = tt->op ();

		if ((ptr[0] == '#') && def_test (ptr)) {
			if (macro_name (ptr, m_name)) 
				functionName = m_name;
			b_inmacro = true;	
		}

		if (! tt->bracketDepth) {
			if (tt->ch == '\n' && ptr[strlen(ptr) - 1] != '\\') {
				b_inmacro = false;
				functionName = oldName;
			}
		}

		if (tt->wit () == OT::Function ) {
			functionName = ww_last_word (tt->op ());
			oldName = functionName;
		}

		if (! tt->bracketDepth)
			continue;

		d_words = split_to_words (ptr);
		for (i = 0; i < d_words->get_size (); i++) {
			S  = cts ((c_word *) d_words->get (i));
			if (! S) 
				continue;

			if (functionName != NULL) {
				printf ("%s\t%s\t%i\t;\tby\t%s\n", S, 
						tt->tt->fileName,
						tt->tt->attachment[ENV->t_op_no].pair_line+1,
						functionName);
			} else {				
				printf ("%s\t%s\t%i\n", S,
					tt->tt->fileName,
					tt->tt->attachment[ENV->t_op_no].pair_line+1);
			}
		}

		d_words->foreach ((Dfunc_t) free_cword);
		delete d_words; 
	}

	fflush (stdout);
	delete tt;
	return 0;
}

int call_tags_multi (EArray * d_files)
{
	char m_buf[512];
	__djob_t * j;
	int i;

	if (! d_files)
		return -1;

	d_files->strings_to_file (ENV->tmp_files);
	split_tmp_files ();

	for (i = 0; i < ENV->max_proc; ++i) {
		j = ENV->proc_list->fork ();
		if (j->child) {
			sprintf (m_buf, "silent_bob -L %s%i --thread --call-tags", ENV->tmp_files, i);
			exit (execlp ("sh", "sh", "-c", m_buf, NULL));
		}
		usleep (500);
	}

	while ((j = ENV->proc_list->wait_all ()) && j) 
		Dexec_done (j);

	mk_tags ("./call_tags", NULL);
	remove_tmp_files ();
	return 0;
}

/// Точка входа для --call-tags
int call_tags (EArray * d_files)
{
	int fd;
	int i;

	if (!d_files || d_files->get_size () == 0) {
		fprintf (stderr, "No such files.\n");
		return -1;
	}

	if (! SB_FLGET (SB_FLTHREAD))
		unlink (ENV->tmp_tags);

	if (ENV->max_proc > 1) 
		return call_tags_multi (d_files);		

	fd = open (ENV->tmp_tags, O_APPEND | O_WRONLY);
	if (fd < 0) {
		fd = open (ENV->tmp_tags, O_CREAT | O_APPEND | O_WRONLY, 0666);
		if (fd < 0)
			return -1;
	}

	dup2 (fd, 1);
	for (i = 0; i < d_files->get_size (); i++)
		callTagsFile (d_files->get (i));
   
	close (fd);
	fclose (stdout);
	if (! SB_FLGET (SB_FLTHREAD)) {
		mk_tags ("./call_tags", NULL);
		remove_tmp_files ();
	}
	
	return 0;
}

