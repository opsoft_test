/*
 * (c) Oleg Puchinin 2006,2007
 * graycardinalster@gmail.com
 * 
 */ 

#include <stdio.h>

void usage ()
{
	printf ("usage:\tsilent_bob [<kernel_function>|<files>] [<options>]\n");
	printf ("\ttags <tag> <tag2> ... <tagN> [<options>]\n");
	printf ("\t options:\n"
			"\t\t -C <dir>\t\t-\tchdir to <dir>\n"
			"\t\t--cgrep\t\t\t-\tsearch in code\n"
			"\t\t--call-tags\t\t-\tcreate \"call_tags\" file\n"
			"\t\t--cfiles\t\t-\tcreate \"cfiles\" file\n"
			"\t\t--ctags-append\t\t-\tdon't rewrite \"tags\" file\n"
			"\t\t--depth <arg>\t\t-\tset reentrant level\n"
			"\t\t--file <files>\t\t-\tshow file functions\n"
			"\t\t -fn\t\t\t-\techo filenames\n"
			"\t\t--structs\t\t-\tshow all structs in stream\n"
			"\t\t -i\t\t\t-\tinteractive mode \"tags\"\n"
			"\t\t--indent\t\t-\tindention\n"
			"\t\t -j\t\t\t-\tnumber of jobs\n"
			"\t\t--kinds\t\t\t-\t(a - all, c - classes, s - structs, f - functions)\n"
			"\t\t--L <file>\t\t-\tread files list from file\n"
			"\t\t--make-ctags\t\t-\tcreate \"tags\" file\n"
			"\t\t--plugins-info\t\t-\tshow available plugins\n"
			"\t\t--the-tt\t\t-\tC preprocessor\n"
			"\t\t -ts\t\t\t-\tforce ctags style\n"
			"\t\t -u\t\t\t-\treverse call-tree\n"
			"\t\t--verbose\t\t-\tprint more other info\n"
			);
	
	printf ("\nCONTACTS\nOleg Puchinin <graycardinalster@gmail.com>\n");
}
