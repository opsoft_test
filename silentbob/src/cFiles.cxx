/*
 * (c) Oleg Puchinin 2006,2007
 * graycardinalster@gmail.com
 *
 */

#include <head.h>

/// Точка входа для --cfiles
int bob_cfiles ()
{
	char m_buf[512];
	unlink ("./cfiles");
	find_cfiles ();
	sprintf (m_buf, "mv \"%s\" ./cfiles", ENV->tmp_files);
	return system (m_buf);
}

