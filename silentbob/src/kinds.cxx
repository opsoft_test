/*
 * (c) Oleg Puchinin 2007
 * graycardinalster@gmail.com
 * 
 */ 

#include <head.h>
#include <TT.h>

int kindsFile (char * fileName, int type)
{
	TT * tt;
	char * op;
	int m_type;

	if (SB_FLGET (SB_FLDEBUG))
		logToFile (ENV->debug_file, "kindsFile: %s %d\n", fileName, type);

	tt = new TT;
	if (tt->loadFile (fileName) < 0) {
		delete tt;
		return -1;
	}

	if (tt->init () < 0) {
		delete tt;
		return -1;
	}

	while (true) {
		op = tt->nextOperator ();
		if (tt->ch == 0)
			break;

		if (! SB_FLGET (SB_FLCPP)) {
			if (tt->bracketDepth)
				continue;
		}

		m_type = tt->wit ();
		if (m_type & type)
			globalsPrint (tt->tt, tt->op (), m_type);
	}

	delete tt;
	return 0;
}



