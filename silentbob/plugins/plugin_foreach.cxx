/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <gclib/gclib.h>
#include <gclib/gclib_c.h>
#include <mod.h>
#include <head.h>
#include <dbg.h>
#include <sys/wait.h>

extern "C" DArray * plugin_init (struct env_t *env);

void __do_foreach (DArray * d_opts, int i)
{
	int count;
	char * cmd;
	char * m_buf;
	int pid;
	int status;
	
	if (! d_opts)
		return;

	count = d_opts->get_size ();
	if (i >= count)
		return;
	cmd = d_opts->get (i);

	m_buf = CNEW (char, 256);
	while (fgets (m_buf, 255, stdin)) {
		chomp (m_buf);
		setenv ("FILE", m_buf, 1);
		pid = fork ();
		if (pid == 0) {
			execlp ("sh", "sh", "-c", cmd, NULL);
			exit (1);
		} else 
			waitpid (pid, &status, 0);
	}
	free (m_buf);
}

char foreach_opt (DArray * d_opts, int * pos)
{
	char * opt;
	int i;
	
	if (! d_opts || ! pos)
		return 0;

	i = *pos;
	opt = d_opts->get (i);
	if (EQ (opt, "--foreach")) {
		__do_foreach (d_opts, ++i);
		exit (0);
	}

	return 0;
}

void foreach_info ()
{
	printf ("For each file.\n");
	printf ("Version: 1.0\n");
	printf ("options: --foreach\n");
}

DArray * plugin_init (struct env_t *env)
{
	DArray * Ret;
	struct mod_feature * pm;

	ENV = env;
	Ret = new DArray (1);
	pm = CNEW (mod_feature, 1);
	memset (pm, 0, sizeof (mod_feature));
	pm->mod.Type = TYPE_FEATURE;
	pm->mod.Version = strdup ("1.0");
	pm->mod.short_info = foreach_info;
	pm->opt = foreach_opt;

	ENV->listOptions->add ("--foreach");

	Ret->add (LPCHAR (pm));
	return Ret;
}

