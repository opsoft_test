/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <gclib/gclib.h>
#include <gclib/gclib_c.h>
#include <gclib/best_names.h>
#include <sys/wait.h>
#include <mod.h>
#include <head.h>
#include <dbg.h>
#include <string>

extern "C" DArray * plugin_init (struct env_t *env);
struct env_t *ENV;

char *__make;
char *__make_cc;
bool __compile_env;
EHash *	__hv_compile_env;

char plugin_make_opt (DArray * d_opts, int * pos)
{
	int count;
	char * S;
	
	if (! d_opts || ! pos)
		return 0;

	count = d_opts->get_size ();
	S = d_opts->get (*pos);

	if (EQ (S, "--make")) {
		if (++(*pos) >= count)
			return 0;

		__make = d_opts->get (*pos);
		return 1;
	}

	if (EQ (S, "--make-env")) {
		__compile_env = true;
		return 1;
	}

	return 0;
}

void __make_dump_files ()
{
	EArray * d_files = ENV->d_files;
	char * S;
	int count;
	int i;
	int k = 0;

	count = d_files->get_size ();
	for (i = 0; i < count; ++i) {
		S = d_files->get (i);
		printf ("%s ", name2obj (d_files->get (i)));
		if (++k == 3) {
			printf ("\\\n\t");			
			k = 0;
		}
	}
	fputc ('\n', stdout);
}

void __make_log (Array * ar)
{
	ar->dump (" ");
	printf ("\n");
}

void __do_make_program () 
{
	Array * ar;
	char *S;
	char * name;
	int status;
	int pid;
	int count;
	int i;

	printf ("PROJECT_OBJECTS=");
	__make_dump_files ();
	printf ("\n");
	printf ("%s : ${PROJECT_OBJECTS}\n", __make);
	printf ("\t${CC} ${LIBS} ${LDFLAGS} -o %s ${PROJECT_OBJECTS}\n", __make);
	printf ("\n");
	fflush (stdout);

	count = ENV->d_files->get_size ();
	for (i = 0; i < count; ++i) {
		name = ENV->d_files->get (i);
		ar = new Array(32);
		(*ar) << __make_cc;
		(*ar) << "-MM";
		ar->expand ((*__hv_compile_env)["INCLUDE"], " ");
		(*ar) << name;
		(*ar) << NULL;
		pid = fork ();
		if (pid == 0)
			execvp (ar->get (0), ar->get_skeleton ());
		else
			waitpid (pid, &status, 0);

		delete ar;
		S = fext (name);
		if (S && EQ (S, ".c"))
			printf ("\t${CC} ${CFLAGS} ${OPTS} ${INCLUDE} -c -o %s %s\n", name2obj (name), name);
		else
			printf ("\t${CXX} ${CXXFLAGS} ${OPTS} ${INCLUDE} -c -o %s %s\n",
				name2obj (name), name);

		printf ("\n");
		fflush (stdout);
	}
	printf ("clean:\n\trm -f ${PROJECT_OBJECTS}\n\trm -f %s\n", __make);
}

void __do_make_shared ()
{
	__make_dump_files ();
}

char plugin_make_opt2 (DArray * d_opts, int * pos)
{
	char * S;
	
	if (! d_opts || ! pos)
		return 0;
	
	S = d_opts->get (*pos);
	if (NE (S, "--make"))
		return 0;
	
	if (! __make)
		exit (1);

	__hv_compile_env = bob_init_compile_env ();
	S = ENV->d_files->get (0);
	S = rindex (S, '.');
	if (! S)
		return 1;
	
	if (__compile_env) {
		printf ("CC=%s\n", (*__hv_compile_env)["CC"]);
		printf ("CXX=%s\n", (*__hv_compile_env)["CXX"]);
		printf ("CFLAGS=%s\n", (*__hv_compile_env)["CFLAGS"]);
		printf ("CXXFLAGS=%s\n", (*__hv_compile_env)["CXXFLAGS"]);
		printf ("INCLUDE=%s\n", (*__hv_compile_env)["INCLUDE"]);
		printf ("OPTS=%s\n", (*__hv_compile_env)["OPTS"]);
		printf ("LDFLAGS=%s\n", (*__hv_compile_env)["LDFLAGS"]);
		printf ("LIBS=%s\n\n", (*__hv_compile_env)["LIBS"]);
	}

	if (EQ (S, ".c"))
		__make_cc = (*__hv_compile_env)["CC"];
	else
		__make_cc = (*__hv_compile_env)["CXX"];

	if (__make) 
		__do_make_program ();

	exit (0);
}

void plugin_make_info ()
{
	printf ("Makefile plugin.\n");
	printf ("Version: 1.0\n");
	printf ("options: --make --make-env\n");
}

DArray * plugin_init (struct env_t *env)
{
	DArray * Ret;
	struct mod_feature * pm;

	__compile_env = false;
	ENV = env;
	Ret = new DArray (1);
	pm = CNEW (mod_feature, 1);
	memset (pm, 0, sizeof (mod_feature));
	pm->mod.Type = TYPE_FEATURE;
	pm->mod.Version = strdup ("1.0");
	pm->mod.short_info = plugin_make_info;
	pm->opt = plugin_make_opt;
	pm->opt2 = plugin_make_opt2;

	ENV->listOptions->add (	"--make");
	ENV->listOptions->add (	"--make-env");

	Ret->add (LPCHAR (pm));
	return Ret;
}

