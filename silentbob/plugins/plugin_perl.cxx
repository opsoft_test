/*
 * (c) Oleg Puchinin 2006.
 * graycardinalster@gmail.com
 *
 * 25/06/06 - Perl plugin for SilentBob.
 * 
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>
#include <gclib/gclib.h>
#include <mod.h>
#include <head.h>
#include <dbg.h>
#include <the_tt.h>
#include <the_fly.hpp>

extern "C" DArray * plugin_init (struct env_t *env);
void __perl_files (char * f_name);
int perl_make_ctags (char * f_name, FILE * of);
char t_op2 (char ** d_in, char ** d_prev);

struct env_t *ENV;
FILE * ofile;
char * PerlPackage;

int perl_print_tags (char * f_name)
{

	return 0;
}

void perl_file (DArray * d_array)
{

}

void pl_sub (struct tt_state_t *tt, int line, char * d_out, char ch)
{
	char *S;
	char *part1, *part2;

	S = strchr (d_out, '(');
	if (S)
		*S = '\0';
	
	part1 = strchr (d_out, ' ');
	++part1;
	part2 = strchr (part1, ':');

	if (part2) {
		*part2 = '\0';
		++part2;
		part2 = strip (part2);
		strip2 (part2);
	}

	strip2 (part1);
	if (part2) 
		fprintf (ofile, "%s:%s\t%s\t%i\n", part1, part2, tt->fileName, line);
	else
		fprintf (ofile, "%s\t%s\t%i\n", part1, tt->fileName, line);
}

void pl_package (struct tt_state_t *tt, int line, char * d_out, char ch)
{
	char * S;
	char * ptr;

	S = strchr (d_out, ' ');
	if (! S)
		return;

	strip2 (S);

	ptr = rindex (S, ':');
	if (ptr) {
		++ptr;
		fprintf (ofile, "%s\t%s\t%i\n", ptr, tt->fileName, line);
	}
	fprintf (ofile, "%s\t%s\t%i\n", S, tt->fileName, line);
}

void perl_make_tag (struct tt_state_t *tt, char * d_out, char ch) 
{
	int line;

	line = tt->attachment[ENV->t_op_no].pair_line+1;
	if (*d_out == ' ')
		++d_out;

	if (ch == '{' && (! strncmp (d_out, "sub ", 4))) {
		pl_sub (tt, line, d_out, ch);
		return;
	}
	
	if (ch == ';' && (! strncmp (d_out, "package ", 8))) {
		pl_package (tt, line, d_out, ch);
		return;
	}
}

void pl_lookup ()
{
	int i;
	DArray * d_array;

	__perl_files (ENV->tmp_files);
	d_array = new DArray (32);
	d_array->from_file (ENV->tmp_files);
	d_array->foreach ((Dfunc_t)chomp);

	for (i = 0; i < d_array->get_size (); ++i) {
		if (! d_array->get (i))
			continue;

		perl_make_ctags (d_array->get (i), ofile);
	}
	
	unlink (ENV->tmp_files);
	d_array->foreach (free);
	delete d_array;
}

int perl_make_ctags (char * f_name, FILE * of) 
{
	char ch;
	char *d_ptr,*d_out; // for t_op2
	int block_depth = 0;
	struct tt_state_t *tt;

	ofile = of;
	if (f_name == NULL) {
		pl_lookup ();
		return 0;
	}
	
	ENV->t_op_no = 0;
	PerlPackage = NULL;
	
	tt = CNEW (tt_state_t, 1);
	memset (tt, 0, sizeof (struct tt_state_t));
	tt->fileName = strdup (f_name);
	THE_FLY::fly_for_file (tt);
	//write (1, tt->d_output, tt->d_output_size);

	d_out = tt->result;
	d_ptr = d_out;
	while (true) {
		ch = t_op2 (&d_ptr, &d_out);
		ENV->t_op_no++;

		if (ch == '\0')
			break;

		if (! block_depth)
			perl_make_tag (tt, d_out, ch);

		if (ch == '{') 
			++block_depth;

		if (ch == '}')
			--block_depth;
		
		if (block_depth < 0)
			block_depth = 0;
	}
	
	free_tt_state (tt);
	return 0;
}

int perl_call_tags (char * f_name)
{
	printf ("Under construction.\n");
	return 0;
}

char perl_opt (DArray * d_opts, int * i) 
{
	if (! d_opts || ! i)
		return 0;

	if (EQ (d_opts->get (*i), "--perl")) {
		ENV->language = "Perl";
		return 1;
	}
	return 0;
}

void __perl_files (char * f_name)
{
	unlink (f_name);
	sblib_find ("./", "*.pm", f_name);
	sblib_find ("./", "*.pl", f_name);
	sblib_find ("./", "*.ph", f_name);
}

char perl_files_opt (DArray * d_opts, int * i) 
{
	if (! d_opts || ! i)
		return 0;
	
	if (EQ (d_opts->get (*i), "--files") && EQ (ENV->language, "Perl")) {
		__perl_files ("./perl_files");
		return 1;
	}

	return 0;
}

void perl_plugin_info ()
{
	printf ("Perl support.\n");
	printf ("Version: 1.0-rc1\n");
	printf ("options: --perl --make-ctags\n");
}

void perl_files_info ()
{
	printf ("Perl files.\n");
	printf ("Version: 1.0\n");
	printf ("options: --perl --files\n");
}

DArray * perl_init ()
{
	DArray * Ret;
	struct mod_feature * pm;
	struct mod_feature * mod_perlfiles;
	struct mod_language * pm_lang;

	Ret = new DArray (2);
	pm = CNEW (mod_feature, 1);
	pm_lang = CNEW (mod_language, 1);
	mod_perlfiles = CNEW (mod_feature, 1);
	
	memset (pm, 0, sizeof (mod_feature));
	memset (pm_lang, 0, sizeof (mod_language));
	memset (mod_perlfiles, 0, sizeof (mod_feature));

	pm->mod.Type = TYPE_FEATURE;
	pm->mod.Version = strdup ("0.1");
	pm->opt = perl_opt;

	mod_perlfiles->mod.Type = TYPE_FEATURE;
	mod_perlfiles->mod.Version = strdup ("1.0");
	mod_perlfiles->mod.short_info = perl_files_info;
	mod_perlfiles->opt = perl_files_opt;
	
	pm_lang->mod.Type = TYPE_LANGUAGE;
	pm_lang->mod.Version = strdup ("1.0-rc1");
	pm_lang->mod.short_info = perl_plugin_info;
	pm_lang->language = strdup ("Perl");
	pm_lang->the = THE_FLY::fly_for_file;
	pm_lang->print_tags = perl_print_tags;
	pm_lang->file = perl_file;
	pm_lang->make_ctags = perl_make_ctags;

	ENV->listOptions->add ("--perl");
	ENV->listOptions->add ("--files");

	Ret->add (LPCHAR (pm));
	Ret->add (LPCHAR (pm_lang));
	Ret->add (LPCHAR (mod_perlfiles));
	
	return Ret;
}

DArray * plugin_init (struct env_t *env)
{
	ENV = env;
#if 1
	return perl_init ();
#else
//	printf ("Perl module under construction.\n");
	return NULL;
#endif
}

