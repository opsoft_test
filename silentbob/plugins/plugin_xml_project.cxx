/*
 * (c) Oleg Puchinin 2007
 * graycardinalster@gmail.com
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <gclib/gclib.h>
#include <gclib/gclib_c.h>
#include <mod.h>
#include <head.h>
#include <dbg.h>

extern "C" DArray * plugin_init (struct env_t *env);
struct env_t *ENV;
char * __xml_project_name;
char * __xml_type;
EHash * __compile_env;

char xml_project_opt (DArray * d_opts, int * pos)
{
	int count;
	char * S;
	
	if (! d_opts || ! pos)
		return 0;

	count = d_opts->get_size ();
	S = d_opts->get (*pos);

	if (EQ (S, "--xml-bin") ||
	    EQ (S, "--xml-program")) {
		if (++(*pos) >= count) 
			return 1;
		__xml_type = "программа";
		__xml_project_name = d_opts->get (*pos);
		return 1;
	}

	if (EQ (S, "--xml-shared")) {
		if (++(*pos) >= count) 
			return 1; 
		__xml_type = "шаровая_лайба";
		__xml_project_name = d_opts->get (*pos);
		return 1;
	}

	return 0;
}

void xmlDump ()
{
	printf ("\t<модуль имя=\"%s\" тип=\"%s\">\n", __xml_project_name, __xml_type);
	printf ("\t<свойство CC=\"%s\" />\n", (*__compile_env)["CC"]);
	printf ("\t<свойство CXX=\"%s\" />\n", (*__compile_env)["CXX"]);
	printf ("\t<свойство OPTS=\"%s\" />\n", (*__compile_env)["OPTS"]);
	printf ("\t<свойство INCLUDE=\"%s\" />\n", (*__compile_env)["INCLUDE"]);
	printf ("\t<свойство LIBS=\"%s\" />\n", (*__compile_env)["LIBS"]);
	printf ("\t<свойство LDFLAGS=\"%s\" />\n", (*__compile_env)["LDFLAGS"]);

	printf ("\t<исходники>\n");
	for (int i = 0; i < ENV->d_files->get_size (); ++i) 
		printf ("\t\t<файл имя=\"%s\" />\n", ENV->d_files->get (i));
	printf ("\t</исходники>\n");
	printf ("\t</модуль>\n");
}

char xml_project_opt2 (DArray * d_opts, int * pos)
{
	char * S;
	
	if (! d_opts || ! pos)
		return 0;
	
	S = d_opts->get (*pos);
	if (EQ (S, "--xml-bin") ||
	    EQ (S, "--xml-program") ||
	    EQ (S, "--xml-shared")) {
		__compile_env = bob_init_compile_env ();
		xmlDump ();
		exit (0);
	}

	return 0;
}

void xml_project_info ()
{
	printf ("XML Project.\n");
	printf ("Version: 1.0\n");
	printf ("options: --xml-project --xml-bin --xml-shared\n");
}

DArray * plugin_init (struct env_t *env)
{
	DArray * Ret;
	struct mod_feature * pm;

	ENV = env;
	Ret = new DArray (1);
	pm = CNEW (mod_feature, 1);
	memset (pm, 0, sizeof (mod_feature));
	pm->mod.Type = TYPE_FEATURE;
	pm->mod.Version = strdup ("1.0");
	pm->mod.short_info = xml_project_info;
	pm->opt = xml_project_opt;
	pm->opt2 = xml_project_opt2;

	Ret->add (LPCHAR (pm));
	return Ret;
}

