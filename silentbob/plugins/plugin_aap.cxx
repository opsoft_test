/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#include <gclib/gclib.h>
#include <sys/wait.h>
#include <mod.h>
#include <head.h>
#include <dbg.h>

extern "C" DArray * plugin_init (struct env_t *env);
EHash * __compile_env;
bool __aap_env;

void __aap_files_join (DArray * d_files)
{
	char *S;
	int count;
	int i;

	if (! d_files)
		return;

	i = 0;
	S = d_files->get (i);
	if (S)
		printf ("%s\n", S);
	else {
		printf ("\n");
		return;
	}
	count = d_files->get_size ();
	while (true) {
		++i;
		S = d_files->get (i);
		if (S)
			printf ("\t\t%s ", S);
		else
			break;

		++i;
		S = d_files->get (i);
		if (S) 
			printf ("%s\n", S);
		else
			break;
	}
	printf ("\n");
}

void __do_aap (char * pr_name, char * type)
{
	DArray * d_files;

	if (! pr_name || ! type)
		return;
	
	d_files = ENV->d_files;
	if (! d_files || d_files->get_size () <= 0)
		return;
	
	__compile_env = bob_init_compile_env ();
	if (! __compile_env)		
		return;
	
	if (__aap_env) {
		printf ("CC=%s\n", __compile_env->get ("CC"));
		printf ("CXX=%s\n", __compile_env->get ("CXX"));
		printf ("CFLAGS=%s\n", __compile_env->get ("CFLAGS"));
		printf ("CXXFLAGS=$CFLAGS\n");
		printf ("OPTS=%s\n", __compile_env->get ("OPTS"));
		printf ("INCLUDE=%s\n", __compile_env->get ("INCLUDE"));
		printf ("LIBS=%s\n", __compile_env->get ("LIBS"));
		printf ("OPTIMIZE=0\n");
		printf ("LDFLAGS=%s\n", __compile_env->get ("LDFLAGS"));
	}

	printf (":%s %s : ", type, pr_name);
	__aap_files_join (d_files);
	printf ("\n");
}

char aap_program_opt2 (DArray * d_opts, int * pos)
{
	int count;
	char * S;
	char * type;
	
	if (! d_opts || ! pos)
		return 0;

	count = d_opts->get_size ();
	S = d_opts->get (*pos);

	if (EQ (S, "--aap-program")) {
		if (++(*pos) >= count)
			return 0;
		type = "program";
		goto letsdoit;
	}

	if (EQ (S, "--aap-dll") ||
	    EQ (S, "--aap-shared")) {
		if (++(*pos) >= count)
			return 0;
		type = "dll";
		goto letsdoit;
	}

	if (EQ (S, "--aap-lib") ||
	    EQ (S, "--aap-static")) {
		if (++(*pos) >= count)
			return 0;
		type = "lib";
		goto letsdoit;
	}

	return 0;

letsdoit:
	S = d_opts->get (*pos);
	__do_aap (S, type);
	exit (0);
	return 1;
}

char aap_program_opt (DArray * d_opts, int * pos)
{
	int count;
	char *S;
	
	if (! d_opts || ! pos)
		return 0;

	count = d_opts->get_size ();
	S = d_opts->get (*pos);
	if (EQ (S, "--aap-program") ||
	    EQ (S, "--aap-lib") ||
	    EQ (S, "--aap-dll") ||
	    EQ (S, "--aap-shared")) {
		if (++(*pos) >= count)
			return 0;
		return 1;
	}

	if (EQ (S, "--aap-env")) {
		__aap_env = true;
		return 1;
	}

	return 0;	
}

void aap_program_info ()
{
	printf ("AAP file.\n");
	printf ("Version: 1.0\n");
	printf ("options: --aap-[program | shared | static] --aap-env\n");
}

DArray * plugin_init (struct env_t *env)
{
	struct mod_feature * pm;
	DArray * Ret;

	__aap_env = false;
	ENV = env;
	Ret = new DArray (1);
	pm = CNEW (mod_feature, 1);
	memset (pm, 0, sizeof (mod_feature));
	pm->mod.Type = TYPE_FEATURE;
	pm->mod.Version = strdup ("0.1");
	pm->mod.short_info = aap_program_info;
	pm->opt = aap_program_opt;
	pm->opt2 = aap_program_opt2;

	ENV->listOptions->add ("--aap-program");
	ENV->listOptions->add ("--aap-shared");
	ENV->listOptions->add ("--aap-static");
	ENV->listOptions->add ("--aap-env");

	Ret->add (LPCHAR (pm));
	return Ret;
}

