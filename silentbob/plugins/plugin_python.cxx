/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#include <gclib/gclib.h>
#include <mod.h>
#include <head.h>
#include <dbg.h>
#include <the_tt.h>
#include <wit.h>

extern "C" DArray * plugin_init (struct env_t *env);
int python_ctags (char * f_name, FILE * of);

namespace PYTHON {
	char * py_tt_file (struct tt_state_t * tt);
	char t_op (char ** d_in, char ** d_prev);
}

char * py_prepare (char *ptr)
{
	char *S;

	S = strchr (ptr, '(');
	if (S)
		*S = '\0';

	strip2 (ptr);
	S = strchr (ptr, ' ');
	return ++S;
}

void python_tag (tt_state_t * tt, char *ptr, char ch, int type, FILE *of)
{
	int line = tt->attachment[ENV->t_op_no].pair_line+1;
	
	while (*ptr == ' ' || *ptr == '\t')
		++ptr;
	
	if (type & OT::Class) {
		if (! strncmp (ptr, "def ", 4)) {
			ptr = py_prepare (ptr);
			goto ttry;
		}
	}

	if (type & OT::Function) {
		if (! strncmp (ptr, "class ", 6)) {
			ptr = py_prepare (ptr);
			goto ttry;
		}
	}
	
	return;
ttry:
	fprintf (of, "%s\t%s\t%i\n", ptr, tt->fileName, line);
}

void python_lookup ()
{
	DArray * d_array;

	unlink (ENV->tmp_files);
	sblib_find ("./", "*.py", ENV->tmp_files);
	d_array = ENV->d_files;	
	d_array->from_file (ENV->tmp_files);
	unlink (ENV->tmp_files);
	d_array->foreach ((Dfunc_t)chomp);
}

void python_ctags_foreach (FILE * of)
{
	int i;
	DArray * d_array = ENV->d_files;
	for (i = 0; i < d_array->get_size (); ++i) {
		if (! d_array->get (i))
			continue;

		python_ctags (d_array->get (i), of);
		free (d_array->get (i));
	}
}

int python_ctags (char * f_name, FILE * of)
{
	char ch;
	char *d_ptr,*d_out; // for t_op2
	struct tt_state_t *tt;

	if (! f_name) {
		python_lookup ();
		python_ctags_foreach (of);
		return 0;
	}

	ENV->t_op_no = 0;
	
	tt = CNEW (tt_state_t, 1);
	memset (tt, 0, sizeof (struct tt_state_t));
	tt->fileName = strdup (f_name);
	PYTHON::py_tt_file (tt);

	d_out = tt->result;
	d_ptr = d_out;
	while (true) {
		ch = PYTHON::t_op (&d_ptr, &d_out);
		++ENV->t_op_no;

		if (ch == '\0')
			break;

		python_tag (tt, d_out, ch, OT::Function | OT::Class, of);
	}
	
	free_tt_state (tt);
	return 0;
}

void pyc_one (char * f_name)
{
	char *S;
	char ch;
	char *d_ptr,*d_out; // for t_op2
	struct tt_state_t *tt;

	tt = CNEW (tt_state_t, 1);
	memset (tt, 0, sizeof (struct tt_state_t));
	tt->fileName = strdup (f_name);
	PYTHON::py_tt_file (tt);

	d_out = tt->result;
	d_ptr = d_out;
	while (true) {
		ch = PYTHON::t_op (&d_ptr, &d_out);

		if (ch == '\0')
			break;

		S = d_out;
		while (*S == ' ' || *S == '\t')
			++S;
		
		if (! strncmp (S, "class ", 6)) 
			printf ("%s\n", py_prepare (S));
	}
	
	free_tt_state (tt);
}

void py_class ()
{
	int i;

	if (! ENV->d_files->get_size ())
		python_lookup ();

	for (i = 0; i < ENV->d_files->get_size (); ++i) 
		pyc_one (ENV->d_files->get (i));

	exit (0);
}

char py_opt (DArray * d_opts, int * i) 
{
	if (! d_opts || ! i)
		return 0;

	if (EQ (d_opts->get (*i), "--python")) {
		ENV->language = strdup ("Python");
		return 1;
	}

	if (EQ (d_opts->get (*i), "--files") && EQ (ENV->language, "Python")) {
		unlink ("./python_files");
		sblib_find ("./", "*.py", "./python_files");
		exit (0);
	}
		
	return 0;
}

char py_opt2 (DArray * d_opts, int * i)
{
	if (EQ (ENV->language, "Python") && EQ (d_opts->get (*i), "--class")) {
		py_class ();
		return 1;
	}
	return 0;
}

void py_info ()
{
	printf ("Python plugin.\n");
	printf ("Version: 1.0\n");
	printf ("options: --python [--files | --make-ctags | --class ]\n");
}

DArray * plugin_init (struct env_t *env) 
{
	struct mod_feature * pm;
	struct mod_language * pm_lang;
	DArray * Ret;

	pm = CNEW (mod_feature, 1);
	pm_lang = CNEW (mod_language, 1);

	memset (pm, 0, sizeof (struct mod_feature));
	memset (pm_lang, 0, sizeof (struct mod_language));
	pm_lang->mod.Version = strdup ("1.0");
	pm_lang->mod.Type = TYPE_LANGUAGE;
	pm_lang->the = PYTHON::py_tt_file;
	pm_lang->make_ctags = python_ctags;
	pm_lang->language = strdup ("Python");
	
	pm->mod.Version = strdup ("1.0");
	pm->opt = py_opt;
	pm->opt2 = py_opt2;
	pm->mod.Type = TYPE_FEATURE;
	pm->mod.short_info = py_info;
	
	ENV->listOptions->add (	"--python");
	ENV->listOptions->add (	"--files");
	ENV->listOptions->add (	"--class");

	Ret = new DArray (2);
	Ret->add (LPCHAR (pm));
	Ret->add (LPCHAR (pm_lang));

	return Ret;
}

