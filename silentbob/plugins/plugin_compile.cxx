/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#include <gclib/gclib.h>
#include <sys/wait.h>
#include <mod.h>
#include <head.h>
#include <dbg.h>

extern "C" DArray * plugin_init (struct env_t *env);
struct env_t *ENV;
EHash * __hv_compile_env;
bool __cc_script;
bool __compile_env;
char * __cc_shared;
char * __cc_program;

void __aa (EArray * a, DArray * b)
{
	int count;
	int i;

	if (! a || ! b)
		return;

	count = b->get_size ();
	for (i = 0; i < count; ++i) 
		a->add (strdup (LPCHAR (b->get (i))));
}

void __astr (EArray * a, char *str)
{
	DArray * d_split;
	
	if (! a || ! str)
		return;

	d_split = Dsplit (str, " ");
	if (! d_split)
		return;
	
	__aa (a, d_split);
	d_split->foreach (free);
	delete d_split;
}

void __array_log (EArray * a)
{
	int i;
	if (! a)
		return;
	for (i = 0; i < a->get_size (); ++i) {
		if (a->get (i))
			printf ("%s ", a->get (i));
	}
	printf ("\n");
}

int compile_cppfile (char * name)
{
	int pid;
	int status;
	char * oname = name2obj (name);
	EArray * d_param;

	if (! oname)
		return -1;
	
	if (__cc_script) {
		printf ("$CXX $CXXFLAGS $OPTS $INCLUDE -c -o %s %s\n", oname, name);
		return 0;
	}

	d_param = new EArray(32);
	d_param->add ( (*__hv_compile_env)["CXX"] );
	__astr (d_param, (*__hv_compile_env)["CXXFLAGS"]);
	if (strlen ((*__hv_compile_env)["OPTS"]))
		__astr (d_param, (*__hv_compile_env)["OPTS"]);
	if (strlen ((*__hv_compile_env)["INCLUDE"]))
		__astr (d_param, (*__hv_compile_env)["INCLUDE"]);

	*d_param << "-c" << "-o" << oname << name << NULL;

	if (SB_FLGET (SB_FLVERBOSE))
		__array_log (d_param);
	else
		printf ("\tCXX\t%s\n", oname);
	
	pid = fork ();
	if (pid == 0)
		execvp ((*__hv_compile_env)["CXX"], d_param->get_skeleton ());
	else 
		waitpid (pid, &status, 0);

	free (oname);	
	return 0;
}

int compile_cfile (char * name)
{
	char * oname = name2obj (name);
	EArray * d_param;
	int status;
	int pid;

	if (! oname)
		return -1;
	
	if (__cc_script) {
		printf ("$CC $CFLAGS $OPTS $INCLUDE -c -o %s %s\n", oname, name);
		return 0;
	}

	d_param = new EArray(32);
	*d_param << (*__hv_compile_env)["CC"];
	__astr (d_param, (*__hv_compile_env)["CFLAGS"]);
	if (strlen ((*__hv_compile_env)["OPTS"]))
		__astr (d_param, (*__hv_compile_env)["OPTS"]);
	if (strlen ( (*__hv_compile_env)["INCLUDE"] ))
		__astr (d_param, (*__hv_compile_env)["INCLUDE"]);

	*d_param << "-c" << "-o" << oname << name << NULL;

	if (SB_FLGET (SB_FLVERBOSE)) 
		__array_log (d_param);
	else
		printf ("\tCC\t%s\n", oname);
	
	pid = fork ();
	if (pid == 0)
		execvp ((*__hv_compile_env)["CC"], d_param->get_skeleton ());
	else 
		waitpid (pid, &status, 0);

	free (oname);	
	return 0;
}

int __cc_build_target ()
{
	EArray * cmd;
	int status;
	char * oname;
	int count;
	pid_t pid;
	int i;

	count = ENV->d_files->get_size ();
	cmd = new EArray (ENV->d_files->get_size ()+32);
	*cmd << (*__hv_compile_env)["CC"];

	if (! __cc_script) {
		__astr (cmd, (*__hv_compile_env)["LDFLAGS"]);
		__astr (cmd, (*__hv_compile_env)["LIBS"]);
	} else {
		*cmd << "$LDFLAGS";
		*cmd << "$LIBS";
	}

	if (__cc_shared)  
		*cmd << "-shared";

	for (i = 0; i < count; ++i) 
		cmd->add (name2obj (ENV->d_files->get (i)));

	if (__cc_shared && __cc_program) {
		fprintf (stderr, "only one --program or --shared option can be used,\n");
		exit (1);
	}

	*cmd << "-o";
	if (__cc_shared)
		oname = __cc_shared;
	else
		oname = __cc_program;

	*cmd << oname;
	*cmd << LPCHAR(NULL);
	
	if (__cc_script || SB_FLGET (SB_FLVERBOSE)) {
		__array_log (cmd);
		return 0;
	} else 
		printf ("\tLD\t%s\n", oname);

	pid = fork ();
	if (pid < 0)
		return -1;

	if (pid == 0)
		execvp ((*cmd)[0], cmd->get_skeleton ());
	else
		waitpid (pid, &status, 0);

	return 0;
}

int compile_files ()
{
	char *S;
	char opts_buf[256];
	int count;
	int i;

	opts_buf[0] = '\0';
	if (__cc_shared && ! strstr ((*__hv_compile_env)["OPTS"], "-fPIC")) {
		strcpy (opts_buf, "-fPIC -DPIC");
		strcat (opts_buf, (*__hv_compile_env)["OPTS"]);
		free ((*__hv_compile_env)["OPTS"]);
		__hv_compile_env->set ("OPTS", opts_buf);
	}

	if (__compile_env) {
		printf ("export CC=\"%s\"\n", (*__hv_compile_env)["CC"]);
		printf ("export CXX=\"%s\"\n", (*__hv_compile_env)["CXX"]);
		printf ("export CFLAGS=\"%s\"\n", (*__hv_compile_env)["CFLAGS"]);
		printf ("export CXXFLAGS=\"%s\"\n", (*__hv_compile_env)["CXXFLAGS"]);
		printf ("export INCLUDE=\"%s\"\n", (*__hv_compile_env)["INCLUDE"]);
		printf ("export OPTS=\"%s\"\n", (*__hv_compile_env)["OPTS"]);
		printf ("export LDFLAGS=\"%s\"\n", (*__hv_compile_env)["LDFLAGS"]);
		printf ("export LIBS=\"%s\"\n\n", (*__hv_compile_env)["LIBS"]);
	}

	count = ENV->d_files->get_size ();
	for (i = 0; i < count; ++i) {
		S = rindex (ENV->d_files->get (i), '.');
		if (! S)
			continue;
		if (EQ (S, ".cpp") || EQ (S, ".cxx"))
			compile_cppfile (ENV->d_files->get (i));
		if (EQ (S, ".c")) 
			compile_cfile (ENV->d_files->get (i));
	}

	if (__cc_shared || __cc_program)
		__cc_build_target ();

	return 0;	
}

char compile_opt (DArray * d_opts, int * pos)
{
	int count;
	char *opt;
	if (! d_opts || ! pos)
		return 0;

	opt = d_opts->get (*pos);
	count = d_opts->get_size ();
	if (EQ (opt, "--cc-script")) {
		__cc_script = true;
		return 1;
	}

	if (EQ (opt, "--cc-shared") ||
	    EQ (opt, "--compile-shared")) {
		++(*pos);
		if (*pos >= count)
			return 0;
		__cc_shared = d_opts->get (*pos);
		return 1;
	}

	if (EQ (opt, "--cc-program") || 
	    EQ (opt, "--compile-program")) {
		++(*pos);
		if (*pos >= count)
			return 0;
		__cc_program = d_opts->get (*pos);
		return 1;
	}

	if (EQ (opt, "--cc-env") ||
	    EQ (opt, "--compile-env")) {
		__compile_env = true;
		return 1;
	}

	return 0;
}

char compile_opt2 (DArray * d_opts, int * pos)
{
	char * S;
	
	if (! d_opts || ! pos)
		return 0;

	S = d_opts->get (*pos);
	if (EQ (S, "--compile") ||
	    EQ (S, "-cc")) {
		__hv_compile_env = bob_init_compile_env ();
		compile_files ();		
		exit (0);
	}

	return 0;
}

void compile_info ()
{
	printf ("C/C++ compilation plugin.\n");
	printf ("Version: 0.3\n");
	printf ("options: -cc --cc-script --cc-shared --cc-program) \n");
}

DArray * plugin_init (struct env_t *env)
{
	DArray * Ret;
	struct mod_feature * pm;
	
	__compile_env = false;
	ENV = env;
	Ret = new DArray (1);
	pm = CNEW (mod_feature, 1);
	memset (pm, 0, sizeof (mod_feature));
	pm->mod.Type = TYPE_FEATURE;
	pm->mod.Version = strdup ("1.0");
	pm->mod.short_info = compile_info;
	pm->opt = compile_opt;
	pm->opt2 = compile_opt2;

	__cc_script = false;
	__cc_shared = NULL;

	ENV->listOptions->add ("--cc-script");
	ENV->listOptions->add ("-compile");
	ENV->listOptions->add ("-cc");
	ENV->listOptions->add ("--cc-shared");
	ENV->listOptions->add ("--compile-shared");
	ENV->listOptions->add ("--cc-program");
	ENV->listOptions->add ("--compile-program");
	ENV->listOptions->add ("--cc-env");
	ENV->listOptions->add ("--compile-env");

	Ret->add (LPCHAR (pm));
	return Ret;
}

