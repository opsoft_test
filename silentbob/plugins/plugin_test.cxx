/*
 * (c) Oleg Puchinin 2007
 * graycardinalster@gmail.com
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <gclib/gclib.h>
#include <gclib/gclib_c.h>
#include <mod.h>
#include <head.h>
#include <dbg.h>
#include <TT.h>
#include <wit.h>

extern "C" DArray * plugin_init (struct env_t *env);
struct env_t *ENV;

char test_plugin_opt (DArray * d_opts, int * pos)
{
	int count;
	char * S;
	
	if (! d_opts || ! pos)
		return 0;

	count = d_opts->get_size ();
	S = d_opts->get (*pos);
	if (EQ (S, "--test")) {
		return 1;
	}

	return 0;
}

int test_plugin_file (char * fileName)
{
	TT * tt;
	char * op;
	int m_type;

	tt = new TT;
	tt->loadFile (fileName);
	tt->init ();

	while (true) {
		op = tt->nextOperator ();
		if (tt->ch == 0)
			break;

	//	if (tt->bracketDepth)
	//		continue;

		m_type = tt->wit ();
		if (m_type & OT::Class)
			globalsPrint (tt->tt, tt->op (), m_type);
	}

	delete tt;
	return 0;
}

char test_plugin_opt2 (DArray * d_opts, int * pos)
{
	char * S;
	int i;
	
	if (! d_opts || ! pos)
		return 0;
	
	S = d_opts->get (*pos);
	if (EQ (S, "--test")) {
		for (i = 0; i < ENV->d_files->get_size (); ++i) 
			test_plugin_file (ENV->d_files->get (i));
		exit (0);
	}

	return 0;
}

void test_plugin_info ()
{
	printf ("Test plugin.\n");
	printf ("Version: 1.0\n");
	printf ("options: --test\n");
}

DArray * plugin_init (struct env_t *env)
{
	DArray * Ret;
	struct mod_feature * pm;

	ENV = env;
	Ret = new DArray (1);
	pm = CNEW (mod_feature, 1);
	memset (pm, 0, sizeof (mod_feature));
	pm->mod.Type = TYPE_FEATURE;
	pm->mod.Version = strdup ("1.0");
	pm->mod.short_info = test_plugin_info;
	pm->opt = test_plugin_opt;
	pm->opt2 = test_plugin_opt2;

	Ret->add (LPCHAR (pm));
	return Ret;
}

