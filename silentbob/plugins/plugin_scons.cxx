/*
 * (c) Oleg Puchinin 2006
 * graycardinalster@gmail.com
 *
 */

#include <gclib/gclib.h>
#include <sys/wait.h>
#include <mod.h>
#include <head.h>
#include <dbg.h>

extern "C" DArray * plugin_init (struct env_t *env);
EHash * __compile_env;
bool __scons_env;

void __scons_files_join (DArray * d_files)
{
	char *S;
	int count;
	int i;

	if (! d_files)
		return;

	i = 0;
	S = d_files->get (i);
	if (S)
		printf ("\'%s\',\n\t", S);
	else {
		printf ("\n");
		return;
	}

	count = d_files->get_size ();
	for (i = 1; i < count; ++i) {
		S = d_files->get (i);
		if (! S)
			break;

		printf ("\'%s\'", S);
		if ((i+1) < count)
			printf (", ");

	}
}

void __print_cpppath ()
{
	EList * list;
	char *S;
	list = new EList;

	printf ("CPPPATH=[");
	Dsplit ( ((*__compile_env) ["INCLUDE"]), " ", list);
	list->first ();
	while ((S = list->get ()) && S) {
		if (strlen (S) > 2) {
			S+=2;
			printf ("'%s'", S);
		}

		if (list->next ()) 
			printf (", ");
	}

	printf ("], ");
	list->foreach ((Dfunc_t)free);
	delete list;
}

void __do_scons (char * pr_name, char * type)
{
	DArray * d_files;

	if (! pr_name || ! type)
		return;
	
	d_files = ENV->d_files;
	if (! d_files || d_files->get_size () <= 0)
		return;
	
	__compile_env = bob_init_compile_env ();
	if (! __compile_env)		
		return;
	
	if (__scons_env) {
		printf ("env = Environment (");
		printf ("CC=\'%s\', ", (*__compile_env) ["CC"]);
		printf ("CXX=\'%s\', ", (*__compile_env) ["CXX"]);
		printf ("CCFLAGS=\'%s\', ", (*__compile_env) ["CFLAGS"]);
		printf ("CXXFLAGS=\'%s\', ", (*__compile_env) ["CXXFLAGS"]);
		printf ("OPTS=\'%s\', ", (*__compile_env) ["OPTS"]);
		__print_cpppath ();
		printf ("LINKFLAGS=\'%s %s\'", (*__compile_env) ["LIBS"], (*__compile_env) ["LDFLAGS"]);
		printf (")\n");
	}
	printf ("env.%s (\"%s\", [", type, pr_name);
	__scons_files_join (d_files);
	printf ("])\n");
}

char scons_program_opt2 (DArray * d_opts, int * pos)
{
	int count;
	char * S;
	char * type;

	if (! d_opts || ! pos)
		return 0;

	count = d_opts->get_size ();
	S = d_opts->get (*pos);

	if (EQ (S, "--scons-program")) {
		if (++(*pos) >= count)
			return 0;
		type = "Program";
		goto letsdoit;
	}

	if (EQ (S, "--scons-shared")) {
		if (++(*pos) >= count)
			return 0;
		type = "SharedLibrary";
		goto letsdoit;
	}

	if (EQ (S, "--scons-static")) {
		if (++(*pos) >= count) 
			return 0;
		type = "Library";
		goto letsdoit;
	}

	return 0;

letsdoit:
	S = d_opts->get (*pos);
	__do_scons (S, type);
	exit (0);
	return 1;
}

char scons_program_opt (DArray * d_opts, int * pos)
{
	int count;
	char *S;
	
	if (! d_opts || ! pos)
		return 0;

	count = d_opts->get_size ();
	S = d_opts->get (*pos);
	if (EQ (S, "--scons-program") ||
	    EQ (S, "--scons-shared")) {
		if (++(*pos) >= count)
			return 0;
		return 1;
	}
	if (EQ (S, "--scons-env")) {
		__scons_env = true;
		return 1;
	}

	return 0;	
}

void scons_program_info ()
{
	printf ("Scons file.\n");
	printf ("Version: 0.1\n");
	printf ("options: --scons-[program | shared | static] --scons-env\n");
}

DArray * plugin_init (struct env_t *env)
{
	struct mod_feature * pm;
	DArray * Ret;
	
	__scons_env = false;
	ENV = env;
	Ret = new DArray (1);
	pm = CNEW (mod_feature, 1);
	memset (pm, 0, sizeof (mod_feature));
	pm->mod.Type = TYPE_FEATURE;
	pm->mod.Version = strdup ("0.1");
	pm->mod.short_info = scons_program_info;
	pm->opt = scons_program_opt;
	pm->opt2 = scons_program_opt2;

	ENV->listOptions->add ("--scons-program");
	ENV->listOptions->add ("--scons-shared");
	ENV->listOptions->add ("--scons-env");
	ENV->listOptions->add ("--scons-static");

	Ret->add (LPCHAR (pm));
	return Ret;
}

